package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.*;
import com.episode6.myworkerapi.model.business.*;
import com.episode6.myworkerapi.model.businessmember.BusinessMemberDetailItem;
import com.episode6.myworkerapi.model.businessmember.BusinessMemberGeneralItem;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.repository.BusinessMemberRepository;
import com.episode6.myworkerapi.repository.BusinessRepository;
import com.episode6.myworkerapi.repository.ContractRepository;
import com.episode6.myworkerapi.repository.RequestRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BusinessService {
    private final BusinessRepository businessRepository;
    private final RequestRepository requestRepository;
    private final ContractRepository contractRepository;
    private final BusinessMemberRepository businessMemberRepository;

    public Business getBusinessData(long id) {
        return businessRepository.findById(id).orElseThrow();
    }

    public Business getBusinessData(Member member) {
        return businessRepository.findByMember(member);
    }

    /**
     * 사업장 등록
     */
    public void setBusiness(BusinessRequest request, Member member) {
        Business business = new Business.Builder(request,member).build();
        businessRepository.save(business);
        requestRepository.save(new Request.Builder(business).build());
    }

    public ListResult<BusinessItem> getBusinessPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Business> businessPage = businessRepository.findByOrderByIdDesc(pageRequest);

        List<BusinessItem> result = new LinkedList<>();
        for (Business business : businessPage) result.add(new BusinessItem.Builder(business).build());
        return ListConvertService.settingResult(result, businessPage.getTotalElements(), businessPage.getTotalPages(), businessPage.getPageable().getPageNumber());
    }

    /**
     * 사업장 상세보기
     */
    public BusinessResponse getBusiness(long id) {
        Business originData = businessRepository.findById(id).orElseThrow();
        Optional<Request> request = requestRepository.findByBusiness(originData);
        return new BusinessResponse.Builder(originData).requestItem(request.orElse(null)).build();

    }

    /**
     * 관리자용 사업장 수정
     */
    public void putBusiness(long id, BusinessChangeRequest request) {
        Business business = businessRepository.findById(id).orElseThrow();
        business.putBusinessChangeRequest(request);
        businessRepository.save(business);
    }

    /**
     * 사장님 사업장 실근무지 수정
     */
    public void putBusinessLocation(Member member, BusinessLocationRequest request) {
        Business business = businessRepository.findBusinessByMember(member);
        business.putReallyLocation(request);
        businessRepository.save(business);
    }

    /**
     * 사장님 내 사업장 보기
     */
    public BusinessResponse getMyBusiness(Member member) {
        Business business = businessRepository.findBusinessByMember(member);
        return new BusinessResponse.Builder(business).build();
    }

    /**
     * 사업장에 해당하는 알바생 리스트
     */
    public BusinessDetailMemberResponse getBusinessOfMember(long id) {
        Business business = businessRepository.findById(id).orElseThrow();

        List<BusinessMember> businessMemberList = businessMemberRepository.findByBusiness(business);

        List<BusinessMemberGeneralItem> itemList = new LinkedList<>();
        for (BusinessMember businessMember : businessMemberList ) {
            Optional<Contract> contract = contractRepository.findByBusinessMember(businessMember);
            itemList.add(new BusinessMemberGeneralItem.Builder(businessMember,contract.orElse(null)).build());
        }
        return new BusinessDetailMemberResponse.Builder(business, itemList).build();
    }
}

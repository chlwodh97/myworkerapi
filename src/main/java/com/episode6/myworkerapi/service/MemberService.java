package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.enums.MemberType;
import com.episode6.myworkerapi.exception.CMemberPasswordException;
import com.episode6.myworkerapi.exception.CMemberUsernameCheckException;
import com.episode6.myworkerapi.exception.CMemberUsernamePatternException;
import com.episode6.myworkerapi.lib.CommonCheck;
import com.episode6.myworkerapi.model.businessmember.BusinessMemberDetailItem;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.member.*;
import com.episode6.myworkerapi.repository.BusinessMemberRepository;
import com.episode6.myworkerapi.repository.BusinessRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.episode6.myworkerapi.repository.MemberRepository;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;
    private final BusinessRepository businessRepository;
    private final BusinessMemberRepository businessMemberRepository;


    /**
     * 아이디값 가져오기
     */
    public Member getMemberData(long id) {
        return memberRepository.findById(id).orElseThrow();
    }

    /**
     *
     * @param username 중복아이디 체크
     * @return 없으면 트루 있으면 펄스
     */
    public MemberDupCheckResponse getMemberIdDupCheck(String username) {
        MemberDupCheckResponse result = new MemberDupCheckResponse();
        result.setIsNew(isNewUsername(username) ? "아이디 생성 가능" : "아이디 중복 생성 불가");

        return result;
    }

    /**
     *
     * @param request 회원가입
     */
    public void setMember(MemberType memberType,MemberRequest request) {
        if (!CommonCheck.checkUsername(request.getUsername())) throw new CMemberUsernamePatternException(); // 유효한 아이디 형식이 아닙니다. 던지기
        if (!request.getPassword().equals(request.getPasswordRe())) throw new CMemberPasswordException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewUsername(request.getUsername())) throw new CMemberUsernameCheckException(); // 중복된 아이디가 존재합니다 던지기

        request.setPassword(passwordEncoder.encode(request.getPassword()));
        Member member = new Member.Builder(memberType, request).build();
        memberRepository.save(member);
    }

    /**
     *
     * @param pageNum 회원 정보 리스트
     * @return 페이징처리된 상태로 리턴 등록순으로 보이게 나중에 추가해야함
     */
    public ListResult<MemberItem> getMembers(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Member> members = memberRepository.findAllByOrderByIdDesc(pageRequest);//수정해하야함
        List<MemberItem> result = new LinkedList<>();

        for (Member member : members) result.add(new MemberItem.Builder(member).build());
        return ListConvertService.settingResult(result, members.getTotalElements(), members.getTotalPages(), members.getPageable().getPageNumber());
    }

    /**
     *
     * @param id 회원 상세보기
     * @return 단수 R 리턴
     */
    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();
        return new MemberResponse.Builder(originData).build();
    }

    /**
     *
     * @param member 토큰으로 받는 아이디
     * @return 내정보 상세보기 리턴
     */
    public MemberResponse getMember(Member member) {
        return new MemberResponse.Builder(member).build();
    }

    /**
     *
     * @param id 회원에 비밀번호
     * @param request 둘다 맞을경우 변경하는데
     * @throws Exception 틀리면 던진다.
     */
    public void putPassword(long id, MemberPasswordCheckRequest request) throws Exception {
        if (!request.getPassword().equals(request.getPasswordRe())) throw new CMemberPasswordException();

        Member uesPassword = memberRepository.findById(id).orElseThrow();
        uesPassword.putPassword(request);
        memberRepository.save(uesPassword);
    }

    /**
     *
     * @param id 회원의 정보를
     * @param request 변경한다.
     */
    public void putMemberChange(long id, MemberChangeRequest request) {
        Member address = memberRepository.findById(id).orElseThrow();
        address.putMemberChange(request);
        memberRepository.save(address);
    }

    /**
     *
     * @param username 아이디 중복 체크
     * @return 숫자로 리턴
     */
    private  boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);

        return dupCount <= 0;
    }

    /**
     * 사장님 회원 최신순 페이징
     */
    public ListResult<MemberItem> getOwnerMember(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);

        Page<Member> memberPage = memberRepository.findAllByMemberTypeOrderByIdDesc(pageRequest, MemberType.ROLE_BOSS);

        List<MemberItem> memberItemList = new LinkedList<>();
        for (Member member : memberPage.getContent()) memberItemList.add(new MemberItem.Builder(member).build());

        return ListConvertService.settingResult(
                memberItemList
                , memberPage.getTotalElements()
                ,memberPage.getTotalPages()
                ,memberPage.getPageable().getPageNumber()
        );
    }

    /**
     * 일반회원 최신순 페이징
     */
    public ListResult<MemberItem> getGeneralMember(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);

        Page<Member> memberPage = memberRepository.findAllByMemberTypeOrderByIdDesc(pageRequest, MemberType.ROLE_GENERAL);

        List<MemberItem> memberItemList = new LinkedList<>();
        for (Member member : memberPage.getContent()) memberItemList.add(new MemberItem.Builder(member).build());

        return ListConvertService.settingResult(
                memberItemList
                , memberPage.getTotalElements()
                ,memberPage.getTotalPages()
                ,memberPage.getPageable().getPageNumber()
        );
    }

    /**
     *
     * @param search 아이디 검색
     * @return 회원 정보
     */
    public MemberResponse getMemberSearch(MemberType memberType, String search) {
        Member member = memberRepository.findByMemberTypeAndUsername(memberType, search);
        return new MemberResponse.Builder(member).build();
    }

    public MemberBossDetailResponse getMyBusinessDetail(Member member) {
        Business business = businessRepository.findByMember(member);
        List<BusinessMember> businessMember = businessMemberRepository.findByBusiness(business);
        int size = businessMember.size();

        return new MemberBossDetailResponse.Builder(member).BusinessDetailResponse(business,size).build();
    }

    public MemberGeneralDetailResponse getMyBusinessGeneralDetail(long id) {
        // 이건 내가 누군지 가져온다.
        Member member = memberRepository.findById(id).orElseThrow();

        // 내가 일하는 내가 일하는 사업장들에 나를 가져왔다.
        List<BusinessMember> businessMember = businessMemberRepository.findByMember(member);

        // 리스트 아이템의 빈그릇을 만들었다.
        List<BusinessMemberDetailItem> detailItems = new LinkedList<>();
        // 사이즈 기본값을 만들어 줬다.
        int size = 0;

        // for문으로 내가 일하는 사업장 수많큼 반복한다.
        for (BusinessMember businessMember1 : businessMember) {
            // 사업장 비지니스로 사업장 멤버들 다 찾아온다.
            // 같은 비즈니스를 가진 줄을뽑아온다.
            List<BusinessMember> businessMemberList = businessMemberRepository.findByBusiness(businessMember1.getBusiness());
            // 찾아온 멤버수를 만들어둔 기본값에 덮어 씌운다.
            size = businessMemberList.size();
            // 만들어둔 빈그릇에 빌더로 담는다.
            detailItems.add(new BusinessMemberDetailItem.Builder(businessMember1, size).build());
        }
        return new MemberGeneralDetailResponse.Builder(member,detailItems).build();
    }
}

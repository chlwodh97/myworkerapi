package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.entity.Schedule;
import com.episode6.myworkerapi.exception.CDistanceOutException;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.schedule.ScheduleEtcRequest;
import com.episode6.myworkerapi.model.schedule.ScheduleItem;
import com.episode6.myworkerapi.model.schedule.ScheduleRequest;
import com.episode6.myworkerapi.repository.BusinessMemberRepository;
import com.episode6.myworkerapi.repository.ScheduleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ScheduleService {
    private final BusinessMemberRepository businessMemberRepository;
    private final ScheduleRepository scheduleRepository;

    /**
     * 사업장 알바
     *
     * @param request 출근 날짜 시간 등록
     */
    public void setSchedule(Business business, Member member, ScheduleRequest request) {
        BusinessMember businessMember = businessMemberRepository.findByBusinessAndMember(business, member);
        // 알바생이 오늘 출근 했는지 안했는지 가져온다.
        Optional<Schedule> schedule = scheduleRepository.findByBusinessMemberAndDateWork(businessMember, LocalDate.now());
        // 지금 현재 시간을 가져온다.
        LocalTime toTime = LocalTime.now();
        // 계약한 출근 시간이랑 현재시간을 비교해 지각을 했는지 안했는지 확인한다.
        boolean bool = toTime.isAfter(LocalTime.parse(strToDayWeek(businessMember)));
        // 출근을 안했으면 등록한다.

        if (distance(businessMember, request) > 20) throw new CDistanceOutException(); {
            if (schedule.isEmpty()) scheduleRepository.save(new Schedule.Builder(businessMember, request, bool).build());
        }

        // 아직 못한거 근무지 gps주소와 현재 위치가 10m이내에 있을 경우만 등록이 된다.
        // 출근 일자 출근 시간 을 ||로 받아서 파싱해서 비교해서 처리하는 방법
    }

    /**
     * 퇴근 하기
     */
    public void putEndWork(Business business, Member member, ScheduleRequest request) {
        BusinessMember businessMember = businessMemberRepository.findByBusinessAndMember(business, member);
        Schedule schedule = scheduleRepository.findByDateWorkAndBusinessMember(LocalDate.now(), businessMember);
        int todayWorkTime = (int) ChronoUnit.HOURS.between(LocalTime.parse(strToDayWeek(businessMember)), LocalTime.now());
        boolean todayWork = todayWorkTime >= 8;

        if (distance(businessMember, request) > 20) throw new CDistanceOutException(); {
            schedule.putEndWork(request, todayWorkTime, todayWork);
            scheduleRepository.save(schedule);
        }
    }

    /**
     * 출근 리스트 관리자용
     */
    public ListResult<ScheduleItem> getSchedules(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Schedule> schedules = scheduleRepository.findAllByOrderByIdDesc(pageRequest);
        List<ScheduleItem> result = new LinkedList<>();
        for (Schedule schedule : schedules) result.add(new ScheduleItem.Builder(schedule).build());
        return ListConvertService.settingResult(result, schedules.getTotalElements(), schedules.getTotalPages(), schedules.getPageable().getPageNumber());
    }

    /**
     * @param business 사업장
     * @param pageNum  보고싶은 페이지
     * @return 사업장 알바생들 출퇴근 기록
     */
    public ListResult<ScheduleItem> getScheduleBusiness(Business business, int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Schedule> schedules = scheduleRepository.findAllByBusinessMember_BusinessOrderByIdDesc(business, pageRequest);
        List<ScheduleItem> result = new LinkedList<>();
        for (Schedule schedule : schedules) result.add(new ScheduleItem.Builder(schedule).build());
        return ListConvertService.settingResult(result, schedules.getTotalElements(), schedules.getTotalPages(), schedules.getPageable().getPageNumber());
    }

    /**
     * 사업장 알바생
     *
     * @param pageNum 보고싶은 페이지
     * @return 알바생의 출퇴근 기록
     */
    public ListResult<ScheduleItem> getScheduleBusinessMember(Business business, Member member, int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        BusinessMember businessMember = businessMemberRepository.findByBusinessAndMember(business, member);
        Page<Schedule> schedules = scheduleRepository.findAllByBusinessMemberOrderByIdDesc(businessMember, pageRequest);
        List<ScheduleItem> result = new LinkedList<>();
        for (Schedule schedule : schedules) result.add(new ScheduleItem.Builder(schedule).build());
        return ListConvertService.settingResult(result, schedules.getTotalElements(), schedules.getTotalPages(), schedules.getPageable().getPageNumber());
    }

    /**
     * 알바생이 다니는 모든 사업장 출퇴근 리스트
     */
    public ListResult<ScheduleItem> getScheduleBusinessMemberAll(Member member, int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Schedule> schedules = scheduleRepository.findAllByBusinessMember_Member(member, pageRequest);
        List<ScheduleItem> result = new LinkedList<>();
        for (Schedule schedule : schedules) result.add(new ScheduleItem.Builder(schedule).build());
        return ListConvertService.settingResult(result, schedules.getTotalElements(), schedules.getTotalPages(), schedules.getPageable().getPageNumber());
    }

    /**
     * 사업장 알바생들 한달 캘린더 스케쥴
     */
    public ListResult<ScheduleItem> getScheduleCalendar(Business business, int year, int month) {
        LocalDate startDate = LocalDate.of(year, month, 1);
        LocalDate endDate = startDate.plusMonths(1).minusDays(1);
        List<Schedule> schedules = scheduleRepository.findAllByBusinessMember_BusinessAndDateWorkBetween(business, startDate, endDate);
        List<ScheduleItem> result = new LinkedList<>();
        for (Schedule schedule : schedules) result.add(new ScheduleItem.Builder(schedule).build());
        return ListConvertService.settingResult(result);
    }

    /**
     * 사업장 캘린더 하루 스케쥴
     */
    public ListResult<ScheduleItem> getScheduleCalendarDay(Business business, LocalDate today) {
        List<Schedule> schedules = scheduleRepository.findAllByBusinessMember_BusinessAndDateWork(business, today);
        List<ScheduleItem> result = new LinkedList<>();
        for (Schedule schedule : schedules) result.add(new ScheduleItem.Builder(schedule).build());
        return ListConvertService.settingResult(result);
    }

    /**
     * 알바생 사업장 하나 캘린더 한달 스케쥴
     */
    public ListResult<ScheduleItem> getScheduleCalendar(Business business, Member member, int year, int month) {
        LocalDate startDate = LocalDate.of(year, month, 1);
        LocalDate endDate = startDate.plusMonths(1).minusDays(1);
        List<Schedule> schedules = scheduleRepository.findAllByBusinessMember_BusinessAndBusinessMember_MemberAndDateWorkBetween(business, member, startDate, endDate);
        List<ScheduleItem> result = new LinkedList<>();
        for (Schedule schedule : schedules) result.add(new ScheduleItem.Builder(schedule).build());
        return ListConvertService.settingResult(result);
    }

    /**
     * 알바생 사업장 하나 캘린더 하루 스케쥴
     */
    public ListResult<ScheduleItem> getScheduleCalendarDay(Business business, Member member, LocalDate today) {
        List<Schedule> schedules = scheduleRepository.findAllByBusinessMember_BusinessAndBusinessMember_MemberAndDateWork(business, member, today);
        List<ScheduleItem> result = new LinkedList<>();
        for (Schedule schedule : schedules) result.add(new ScheduleItem.Builder(schedule).build());
        return ListConvertService.settingResult(result);
    }

    /**
     * 메모 등록 및 수정
     */
    public void putScheduleEtc(long id, ScheduleEtcRequest request) {
        Schedule schedule = scheduleRepository.findById(id).orElseThrow();

        schedule.putEtc(request);
        scheduleRepository.save(schedule);
    }

    /**
     * @param id 출퇴근 삭제
     */
    public void delSchedule(long id) {
        scheduleRepository.deleteById(id);
    }

    /**
     * @return 오늘 요일
     */
    private String strToDayWeek(BusinessMember businessMember) {
        // 비즈니스멤버에 출근요일이랑 시간이 있다.
        // 그걸 가지고 출근요일이 지금 출근 요일이랑 같을때 같은 배열에 있는 출근 시간을 써서 지각 여부를 판단해야한다.
        // java 값으로 인덱스 번호 찾기
        int toWeek = LocalDate.now().getDayOfWeek().getValue();
        String[] weekText = new String[] {"월", "화", "수", "목", "금", "토", "일"};
        String todayText = weekText[toWeek - 1];

        String weekSchedule = businessMember.getWeekSchedule();
        String weekTime = businessMember.getTimeScheduleStart();
        String[] today = weekSchedule.split("@");
        String[] startTimes = weekTime.split("@");
        int index = Arrays.asList(today).indexOf(todayText);
        String startTime = startTimes[index];

        return startTime;
    }

    /**
     *
     * @param businessMember 근무지 위도 경도를 줘야한다.
     * @param request 출근할 위치 위도 경도를 줘야한다.
     * @return 그럼 두 거리를 m로 리턴한다.
     */
    private double distance(BusinessMember businessMember, ScheduleRequest request) {
        double lat1 = businessMember.getBusiness().getLatitudeBusiness();
        double lon1 = businessMember.getBusiness().getLongitudeBusiness();
        double lat2 = request.getLatitude();
        double lon2 = request.getLongitude();
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));

        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515 * 1609.344;

        return dist;
    }

    /**
     *
     * @param deg 10진수를 받아서
     * @return 라디안으로 리턴한다.
     */
    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /**
     *
     * @param rad 라디안을 받아서
     * @return 10진수를 리턴한다.
     */
    private double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }
}

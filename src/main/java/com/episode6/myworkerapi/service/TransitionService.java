package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.entity.Transition;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.transition.TransitionItem;
import com.episode6.myworkerapi.model.transition.TransitionRequest;
import com.episode6.myworkerapi.model.transition.TransitionResponse;
import com.episode6.myworkerapi.repository.TransitionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TransitionService {
    private final TransitionRepository transitionRepository;

    /**
     *
     * @param request 인수인계 등록
     */
    public void setTransition(Business business, Member member, TransitionRequest request) {
        transitionRepository.save(new Transition.Builder(business, member, request).build());
    }

    /**
     *
     * @param id 인수인계 처리 완료 버튼
     */
    public void putFinish(long id) {
        Transition originData = transitionRepository.findById(id).orElseThrow();
        originData.putFinish();
        transitionRepository.save(originData);
    }

    /**
     *
     * @param pageNum 인수인계 리스트 페이징
     */
    public ListResult<TransitionItem> getTransitions(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Transition> transitions = transitionRepository.findAllByOrderByIdDesc(pageRequest);
        List<TransitionItem> result = new LinkedList<>();

        for (Transition transition : transitions) result.add(new TransitionItem.Builder(transition).build());
        return ListConvertService.settingResult(result, transitions.getTotalElements(), transitions.getTotalPages(), transitions.getPageable().getPageNumber());
    }

    /**
     * 사업장 인수인계
     */
    public ListResult<TransitionItem> getTransitionBusiness(Business business, int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Transition> transitions = transitionRepository.findAllByBusinessOrderByIdDesc(business, pageRequest);
        List<TransitionItem> result = new LinkedList<>();

        for (Transition transition : transitions) result.add(new TransitionItem.Builder(transition).build());
        return ListConvertService.settingResult(result, transitions.getTotalElements(), transitions.getTotalPages(), transitions.getPageable().getPageNumber());
    }

    /**
     * 캘린더 한달 내용 가져오기
     */
    public ListResult<TransitionItem> getTransitionBusiness(Business business, int year, int month) {
        LocalDateTime startTime = LocalDateTime.of(year, month, 1, 0, 0);
        LocalDateTime endTime = startTime.plusMonths(1).minusSeconds(1);
        List<Transition> transitions = transitionRepository.findAllByBusinessAndDateTransitionBetween(business, startTime, endTime);
        List<TransitionItem> result = new LinkedList<>();
        for (Transition transition : transitions) result.add(new TransitionItem.Builder(transition).build());
        return ListConvertService.settingResult(result);
    }

    /**
     * 캘린더 하루 내용 가져오기
     */
    public ListResult<TransitionItem> getTransitionBusiness(Business business, LocalDate date) {
        LocalDateTime startTime = LocalDateTime.of(date.getYear(), date.getMonth(),date.getDayOfMonth(), 0, 0);
        LocalDateTime endTime = startTime.plusDays(1).minusSeconds(1);
        List<Transition> transitions = transitionRepository.findAllByBusinessAndDateTransitionBetween(business, startTime, endTime);
        List<TransitionItem> result = new LinkedList<>();
        for (Transition transition : transitions) result.add(new TransitionItem.Builder(transition).build());
        return ListConvertService.settingResult(result);
    }

    /**
     *
     * @param id 인수인계 상세보기
     */
    public TransitionResponse getTransition(long id) {
        Transition originData = transitionRepository.findById(id).orElseThrow();
        return new TransitionResponse.Builder(originData).build();
    }

    /**
     * 인수인계 수정하는데 처리 완료상태면 수정불가
     */
    public void putTransition(long id, TransitionRequest request) {
        Transition originData = transitionRepository.findById(id).orElseThrow();

        if (!originData.getIsFinish()) {
            originData.putContent(request);
            transitionRepository.save(originData);
        }
    }

    /**
     *
     * @param id 인수인계 삭제
     */
    public void delTransition(long id) {
        transitionRepository.deleteById(id);
    }
}

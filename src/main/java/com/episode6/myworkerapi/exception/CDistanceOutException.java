package com.episode6.myworkerapi.exception;

public class CDistanceOutException extends RuntimeException{
    public CDistanceOutException(String msg, Throwable t) {
        super(msg,t);
    }
    public CDistanceOutException(String msg) {
        super(msg);
    }
    public CDistanceOutException() {
        super();
    }
}

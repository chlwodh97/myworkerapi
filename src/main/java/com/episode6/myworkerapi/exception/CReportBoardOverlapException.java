package com.episode6.myworkerapi.exception;

public class CReportBoardOverlapException extends RuntimeException{
    public CReportBoardOverlapException(String msg, Throwable t) {
        super(msg,t);
    }
    public CReportBoardOverlapException(String msg) {
        super(msg);
    }
    public CReportBoardOverlapException() {
        super();
    }
}


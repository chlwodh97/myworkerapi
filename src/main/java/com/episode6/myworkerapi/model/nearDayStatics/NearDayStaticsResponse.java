package com.episode6.myworkerapi.model.nearDayStatics;


import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class NearDayStaticsResponse {
    private List<Long> joinDatasets;
    private List<Long> businessDatasets;
    private List<Long> newWriteDatasets;
    private List<Long> newInquiryDatasets;
}

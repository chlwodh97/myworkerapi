package com.episode6.myworkerapi.model.comment;


import com.episode6.myworkerapi.entity.Comment;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;

@Getter
@Schema
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CommentDetailItem {
    @Schema(description = "멤버 id")
    private Long memberId;
    @Schema(description = "멤버 이름")
    private String memberName;
    @Schema(description = "댓글 내용")
    private String content;
    @Schema(description = "댓글 작성일")
    private String DateComment;


    private CommentDetailItem(Builder builder) {
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.content = builder.content;
        this.DateComment = builder.DateComment;

    }

    public static class Builder implements CommonModelBuilder<CommentDetailItem> {

        private final Long memberId;
        private final String memberName;
        private final String content;
        private final String DateComment;


        public Builder(Comment comment) {
            this.memberId = comment.getMember().getId();
            this.memberName = comment.getMember().getName();
            this.content = comment.getContent();
            this.DateComment = comment.getDateContent().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }

        @Override
        public CommentDetailItem build() {
            return new CommentDetailItem(this);
        }
    }
}
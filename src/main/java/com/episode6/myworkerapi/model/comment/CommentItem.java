package com.episode6.myworkerapi.model.comment;


import com.episode6.myworkerapi.entity.Comment;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.format.DateTimeFormatter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CommentItem {
    private Long id;
    @Schema(description = "댓글 내용")
    private String content;
    @Schema(description = "작성 날짜")
    private String dateContent;
    @Schema(description = "멤버 id")
    private Long memberId;
    @Schema(description = "게시판 id")
    private Long boardId;

    private CommentItem(Builder builder) {
        this.id = builder.id;
        this.content = builder.content;
        this.dateContent = builder.dateContent;
        this.memberId = builder.memberId;
        this.boardId = builder.boardId;
    }

    public static class Builder implements CommonModelBuilder<CommentItem> {
        private final Long id;
        private final String content;
        private final String dateContent;

        private final Long memberId;

        private final Long boardId;

        public Builder(Comment comment) {
            this.id = comment.getId();
            this.content = comment.getContent();
            this.dateContent = comment.getDateContent().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));
            this.memberId = comment.getMember().getId();
            this.boardId = comment.getBoard().getId();
        }
        @Override
        public CommentItem build() {
            return new CommentItem(this);
        }
    }
}

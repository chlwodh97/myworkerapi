package com.episode6.myworkerapi.model.memberask;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberAskRequest {

    @Schema(description = "문의 제목")
    private String title;

    @Schema(description = "문의 내용")
    private String content;
}

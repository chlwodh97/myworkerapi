package com.episode6.myworkerapi.model.memberask;

import com.episode6.myworkerapi.entity.AskAnswer;
import com.episode6.myworkerapi.entity.MemberAsk;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.askanswer.AskAnswerComment;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberAskResponse {
    @Schema(description = "시퀀스")
    private Long id;
    @Schema(description = "멤버 시퀀스")
    private Long memberId;
    @Schema(description = "멤버 이름")
    private String memberName;
    @Schema(description = "문의 처리 상태")
    private String isAnswer;
    @Schema(description = "문의 제목")
    private String title;
    @Schema(description = "문의 내용")
    private String content;
    @Schema(description = "문의 첨부 이미지")
    private String questionImgUrl;
    @Schema(description = "문의 등록 날짜 시간")
    private String dateMemberAsk;

    @Schema(description = "관리자 시퀀스")
    private AskAnswerComment comment;

    private MemberAskResponse(Builder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.isAnswer = builder.isAnswer;
        this.title = builder.title;
        this.content = builder.content;
        this.questionImgUrl = builder.questionImgUrl;
        this.dateMemberAsk = builder.dateMemberAsk;

        this.comment = builder.comment;
    }

    public static class Builder implements CommonModelBuilder<MemberAskResponse> {
        private final Long id;
        private final Long memberId;
        private final String memberName;
        private final String isAnswer;
        private final String title;
        private final String content;
        private final String questionImgUrl;
        private final String dateMemberAsk;

        private AskAnswerComment comment;

        public Builder(MemberAsk memberAsk) {
            this.id = memberAsk.getId();
            this.memberId = memberAsk.getMember().getId();
            this.memberName = memberAsk.getMember().getName();
            this.isAnswer = memberAsk.getIsAnswer() ? "답변 완료" : "미답변";
            this.title = memberAsk.getTitle();
            this.content = memberAsk.getContent();
            this.questionImgUrl = memberAsk.getQuestionImgUrl();
            this.dateMemberAsk = memberAsk.getDateMemberAsk().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));
        }

        public Builder comment(AskAnswer askAnswer) {
            this.comment = askAnswer == null ? null : new AskAnswerComment.Builder(askAnswer).build();
            return this;
        }

        @Override
        public MemberAskResponse build() {
            return new MemberAskResponse(this);
        }
    }
}

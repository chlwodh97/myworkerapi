package com.episode6.myworkerapi.model.schedule;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScheduleEtcRequest {
    private String etc;
}

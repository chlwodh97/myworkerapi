package com.episode6.myworkerapi.model.schedule;

import com.episode6.myworkerapi.entity.Schedule;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ScheduleItem {
    @Schema(description = "스퀀스")
    private Long id;
    @Schema(description = "비즈니스멤버 스퀀스")
    private Long businessMemberId;
    @Schema(description = "비즈니스멤버 직책")
    private String businessMemberPosition;
    @Schema(description = "비즈니스멤버 이름")
    private String businessMemberName;
    @Schema(description = "0000년00월00일")
    private String dateWork;
    @Schema(description = "출근 시간")
    private String startWork;
    @Schema(description = "퇴근 시간")
    private String endWork;
    @Schema(description = "당일 근무 시간")
    private Integer totalWorkTime;
    @Schema(description = "지각 여부")
    private String isLateness;
    @Schema(description = "잔업 여부")
    private String isOverTime;
    @Schema(description = "메모")
    private String etc;

    private ScheduleItem(Builder builder) {
        this.id = builder.id;
        this.businessMemberId = builder.businessMemberId;
        this.businessMemberPosition = builder.businessMemberPosition;
        this.businessMemberName = builder.businessMemberName;
        this.dateWork = builder.dateWork;
        this.startWork = builder.startWork;
        this.endWork = builder.endWork;
        this.totalWorkTime = builder.totalWorkTime;
        this.isLateness = builder.isLateness;
        this.isOverTime = builder.isOverTime;
        this.etc = builder.etc;
    }

    public static class Builder implements CommonModelBuilder<ScheduleItem> {
        private final Long id;
        private final Long businessMemberId;
        private final String businessMemberPosition;
        private final String businessMemberName;
        private final String dateWork;
        private final String startWork;
        private final String endWork;
        private final Integer totalWorkTime;
        private final String isLateness;
        private final String isOverTime;
        private final String etc;

        public Builder(Schedule schedule) {
            this.id = schedule.getId();
            this.businessMemberId = schedule.getBusinessMember().getId();
            this.businessMemberPosition = schedule.getBusinessMember().getPosition().getName();
            this.businessMemberName = schedule.getBusinessMember().getMember().getName();
            this.dateWork = schedule.getDateWork().format(DateTimeFormatter.ofPattern("yyyy.MM.dd"));
            this.startWork = schedule.getStartWork().format(DateTimeFormatter.ofPattern("HH:mm"));
            this.endWork = schedule.getEndWork() == null ? null : schedule.getEndWork().format(DateTimeFormatter.ofPattern("HH:mm"));
            this.totalWorkTime = schedule.getTotalWorkTime();
            this.isLateness = schedule.getIsLateness() ? "지각" : "정상출근";
            this.isOverTime = schedule.getIsOverTime() ? "잔업" : " - ";
            this.etc = schedule.getEtc();
        }
        @Override
        public ScheduleItem build() {
            return new ScheduleItem(this);
        }
    }
}

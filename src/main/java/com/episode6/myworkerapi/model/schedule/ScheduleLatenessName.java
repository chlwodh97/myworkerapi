package com.episode6.myworkerapi.model.schedule;

import com.episode6.myworkerapi.entity.Schedule;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ScheduleLatenessName {
    private String businessMemberName;

    private ScheduleLatenessName(Builder builder) {
        this.businessMemberName = builder.businessMemberName;
    }

    public static class Builder implements CommonModelBuilder<ScheduleLatenessName> {
        private final String businessMemberName;

        public Builder(Schedule schedule) {
            this.businessMemberName = schedule.getBusinessMember().getMember().getName();
        }

        @Override
        public ScheduleLatenessName build() {
            return new ScheduleLatenessName(this);
        }
    }
}

package com.episode6.myworkerapi.model.schedule;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScheduleRequest {
    @NotNull
    @Schema(description = "위도")
    private Double Latitude;
    @NotNull
    @Schema(description = "경도")
    private Double Longitude;
}

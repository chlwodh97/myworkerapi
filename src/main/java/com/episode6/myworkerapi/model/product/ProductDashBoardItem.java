package com.episode6.myworkerapi.model.product;

import com.episode6.myworkerapi.entity.Product;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Schema
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductDashBoardItem {
    private Long id;
    @Schema(description = "상품이름")
    private String productName;
    @Schema(description = "최소수량")
    private Short minQuantity;
    @Schema(description = "현재수량")
    private Short nowQuantity;

    private ProductDashBoardItem(Builder builder) {
        this.id = builder.id;
        this.productName = builder.productName;
        this.minQuantity = builder.minQuantity;
        this.nowQuantity = builder.nowQuantity;
    }

    public static class Builder implements CommonModelBuilder<ProductDashBoardItem> {
        private final Long id;
        private final String productName;
        private final Short minQuantity;
        private final Short nowQuantity;

        public Builder(Product product) {
            this.id = product.getId();
            this.productName = product.getProductName();
            this.minQuantity = product.getMinQuantity();
            this.nowQuantity = product.getNowQuantity();
        }

        @Override
        public ProductDashBoardItem build() {
            return new ProductDashBoardItem(this);
        }
    }
}

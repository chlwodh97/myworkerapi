package com.episode6.myworkerapi.model.askanswer;

import com.episode6.myworkerapi.entity.AskAnswer;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AskAnswerComment {
    @Schema(description = "관리자 시퀀스")
    private Long adminId;
    @Schema(description = "관리자 이름")
    private String adminName;
    @Schema(description = "답변 제목")
    private String answerTitle;
    @Schema(description = "답변 내용")
    private String answerContent;
    @Schema(description = "답변 추가 이미지 ")
    private String answerImgUrl;
    @Schema(description = "문의 답변 등록 날짜")
    private String dateAnswer;

    private AskAnswerComment(Builder builder) {
        this.adminId = builder.adminId;
        this.adminName = builder.adminName;
        this.answerTitle = builder.answerTitle;
        this.answerContent = builder.answerContent;
        this.answerImgUrl = builder.answerImgUrl;
        this.dateAnswer = builder.dateAnswer;
    }

    public static class Builder implements CommonModelBuilder<AskAnswerComment> {
        private final Long adminId;
        private final String adminName;
        private final String answerTitle;
        private final String answerContent;
        private final String answerImgUrl;
        private final String dateAnswer;

        public Builder(AskAnswer askAnswer) {
            this.adminId = askAnswer.getMember().getId();
            this.adminName = askAnswer.getMember().getName();
            this.answerTitle = askAnswer.getAnswerTitle();
            this.answerContent = askAnswer.getAnswerContent();
            this.answerImgUrl = askAnswer.getAnswerImgUrl();
            this.dateAnswer = askAnswer.getDateAnswer().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));
        }

        @Override
        public AskAnswerComment build() {
            return new AskAnswerComment(this);
        }
    }
}

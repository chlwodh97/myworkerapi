package com.episode6.myworkerapi.model.askanswer;

import com.episode6.myworkerapi.entity.AskAnswer;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AskAnswerItem {
    @Schema(description = "스퀀스")
    private Long id;
    @Schema(description = "관리자 시퀀스")
    private Long memberId;
    @Schema(description = "관리자 이름")
    private String memberName;
    @Schema(description = "문의 내역 시퀀스")
    private Long memberAskId;
    @Schema(description = "문의한 멤버 이름")
    private String memberAskMemberName;
    @Schema(description = "문의 내역 제목")
    private String memberAskTitle;
    @Schema(description = "답변 제목")
    private String answerTitle;
    @Schema(description = "문의 답변 등록 날짜")
    private String dateAnswer;

    private AskAnswerItem(Builder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.memberAskId = builder.memberAskId;
        this.memberAskMemberName = builder.memberAskMemberName;
        this.memberAskTitle = builder.memberAskTitle;
        this.answerTitle = builder.answerTitle;
        this.dateAnswer = builder.dateAnswer;
    }

    public static class Builder implements CommonModelBuilder<AskAnswerItem> {
        private final Long id;
        private final Long memberId;
        private final String memberName;
        private final Long memberAskId;
        private final String memberAskMemberName;
        private final String memberAskTitle;
        private final String answerTitle;
        private final String dateAnswer;

        public Builder(AskAnswer askAnswer) {
            this.id = askAnswer.getId();
            this.memberId = askAnswer.getMember().getId();
            this.memberName = askAnswer.getMember().getName();
            this.memberAskId = askAnswer.getMemberAsk().getId();
            this.memberAskMemberName = askAnswer.getMemberAsk().getMember().getName();
            this.memberAskTitle = askAnswer.getMemberAsk().getTitle();
            this.answerTitle = askAnswer.getAnswerTitle();
            this.dateAnswer = askAnswer.getDateAnswer().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));
        }
        @Override
        public AskAnswerItem build() {
            return new AskAnswerItem(this);
        }
    }
}

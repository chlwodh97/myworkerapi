package com.episode6.myworkerapi.model.request;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Schema
public class RefuseReasonRequest {
    @Schema(description = "관리자 반려사유")
    private String refuseReason;
}

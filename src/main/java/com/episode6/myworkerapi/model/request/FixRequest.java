package com.episode6.myworkerapi.model.request;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FixRequest {
    @Schema(description = "상호명")
    private String businessName;
    @Schema(description = "대표자명")
    private String ownerName;
    @Schema(description = "사업장 이미지")
    private String businessImgUrl;
    @Schema(description = "업종")
    private String businessType;
    @Schema(description = "소재지")
    private String businessLocation;
    @Schema(description = "이메일")
    private String businessEmail;
    @Schema(description = "전화번호")
    private String businessPhoneNumber;
    @Schema(description = "실근무지 주소")
    private String reallyLocation;
    @Schema(description = "비고")
    private String ectMemo;
}

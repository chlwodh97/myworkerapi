package com.episode6.myworkerapi.model.request;

import com.episode6.myworkerapi.entity.RequestFix;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FixResponse {
    private Long id;
    @Schema(description = "사업장 id")
    private Long businessId;
    @Schema(description = "상호명")
    private String businessName;
    @Schema(description = "대표자명")
    private String ownerName;
    @Schema(description = "사업장 이미지")
    private String businessImgUrl;
    @Schema(description = "업종")
    private String businessType;
    @Schema(description = "소재지")
    private String businessLocation;
    @Schema(description = "이메일")
    private String businessEmail;
    @Schema(description = "전화번호")
    private String businessPhoneNumber;
    @Schema(description = "실근무지 주소")
    private String reallyLocation;
    @Schema(description = "비고")
    private String etcMemo;

    private FixResponse(Builder builder) {
        this.id = builder.id;
        this.businessId = builder.businessId;
        this.businessName = builder.businessName;
        this.ownerName = builder.ownerName;
        this.businessImgUrl = builder.businessImgUrl;
        this.businessType = builder.businessType;
        this.businessLocation = builder.businessLocation;
        this.businessEmail = builder.businessEmail;
        this.businessPhoneNumber = builder.businessPhoneNumber;
        this.reallyLocation = builder.reallyLocation;
        this.etcMemo = builder.etcMemo;
    }

    public static class Builder implements CommonModelBuilder<FixResponse> {
        private final Long id;
        private final Long businessId;
        private final String businessName;
        private final String ownerName;
        private final String businessImgUrl;
        private final String businessType;
        private final String businessLocation;
        private final String businessEmail;
        private final String businessPhoneNumber;
        private final String reallyLocation;
        private final String etcMemo;


        public Builder(RequestFix fix) {
            this.id = fix.getId();
            this.businessId = fix.getBusiness().getId();
            this.businessName = fix.getBusinessName();
            this.ownerName = fix.getOwnerName();
            this.businessImgUrl = fix.getBusinessImgUrl();
            this.businessType = fix.getBusinessType();
            this.businessLocation = fix.getBusinessLocation();
            this.businessEmail = fix.getBusinessEmail();
            this.businessPhoneNumber = fix.getBusinessPhoneNumber();
            this.reallyLocation = fix.getReallyLocation();
            this.etcMemo = fix.getEtcMemo();
        }
        @Override
        public FixResponse build() {
            return new FixResponse(this);
        }
    }
}

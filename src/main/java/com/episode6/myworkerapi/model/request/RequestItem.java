package com.episode6.myworkerapi.model.request;

import com.episode6.myworkerapi.entity.Request;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RequestItem {
    private Long id;
    @Schema(description = "사업장 id")
    private Long BusinessId;
    @Schema(description = "요청일시")
    private String dateRequest;


    private RequestItem(Builder builder) {
        this.id = builder.id;
        this.BusinessId = builder.BusinessId;
        this.dateRequest = builder.dateRequest;
    }


    public static class Builder implements CommonModelBuilder<RequestItem> {
        private final Long id;
        private final Long BusinessId;
        private final String dateRequest;


        public Builder(Request request) {
            this.id = request.getId();
            this.BusinessId = request.getBusiness().getId();
            this.dateRequest = request.getDateRequest().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));
        }
        @Override
        public RequestItem build() {
            return new RequestItem(this);
        }
    }

}

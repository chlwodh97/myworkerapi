package com.episode6.myworkerapi.model.businessmember;

import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;
import java.util.Arrays;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BusinessMemberItem {
    @Schema(description = "시퀀스")
    private Long id;
    @Schema(description = "알바생 시퀀스")
    private Long memberId;
    @Schema(description = "알바생 이름")
    private String memberName;
    @Schema(description = "사업장 시퀀스")
    private Long businessId;
    @Schema(description = "사업장 이름")
    private String businessName;
    @Schema(description = "직책")
    private String position;
    @Schema(description = "퇴직여부")
    private String isWork;
    @Schema(description = "입사일")
    private String dateIn;
    @Schema(description = "퇴사일")
    private String dateOut;
    @Schema(description = "메모")
    private String etc;
    @Schema(description = "근무 요일")
    private String weekSchedule;
    @Schema(description = "근무 시작 시간")
    private String timeScheduleStart;
    @Schema(description = "근무 퇴근 시간")
    private String timeScheduleEnd;
    @Schema(description = "총 근무 시간")
    private String timeScheduleTotal;

    private BusinessMemberItem(Builder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.businessId = builder.businessId;
        this.businessName = builder.businessName;
        this.position = builder.position;
        this.isWork = builder.isWork;
        this.dateIn = builder.dateIn;
        this.dateOut = builder.dateOut;
        this.etc = builder.etc;
        this.weekSchedule = builder.weekSchedule;
        this.timeScheduleStart = builder.timeScheduleStart;
        this.timeScheduleEnd = builder.timeScheduleEnd;
        this.timeScheduleTotal = builder.timeScheduleTotal;
    }

    public static class Builder implements CommonModelBuilder<BusinessMemberItem> {
        private final Long id;
        private final Long memberId;
        private final String memberName;
        private final Long businessId;
        private final String businessName;
        private final String position;
        private final String isWork;
        private final String dateIn;
        private final String dateOut;
        private final String etc;
        private final String weekSchedule;
        private final String timeScheduleStart;
        private final String timeScheduleEnd;
        private final String timeScheduleTotal;

        public Builder(BusinessMember businessMember) {
            this.id = businessMember.getId();
            this.memberId = businessMember.getMember().getId();
            this.memberName = businessMember.getMember().getName();
            this.businessId = businessMember.getBusiness().getId();
            this.businessName = businessMember.getBusiness().getBusinessName();
            this.position = businessMember.getPosition().getName();
            this.isWork = businessMember.getIsWork() ? "재직중" : "퇴직";
            this.dateIn = businessMember.getDateIn().format(DateTimeFormatter.ofPattern("yyyy.MM.dd"));
            this.dateOut = businessMember.getDateOut() == null ? null : businessMember.getDateOut().format(DateTimeFormatter.ofPattern("yyyy.MM.dd"));
            this.etc = businessMember.getEtc();
            this.weekSchedule = businessMember.getWeekSchedule() == null ? null : businessMember.getWeekSchedule().replace("@", ",");
            this.timeScheduleStart = businessMember.getTimeScheduleStart() == null ? null : businessMember.getTimeScheduleStart().replace("@", ",");
            this.timeScheduleEnd = businessMember.getTimeScheduleEnd() == null ? null : businessMember.getTimeScheduleEnd().replace("@", ",");
            this.timeScheduleTotal = businessMember.getTimeScheduleTotal() == null ? null : businessMember.getTimeScheduleTotal().replace("@", ",");
        }
        @Override
        public BusinessMemberItem build() {
            return new BusinessMemberItem(this);
        }
    }
}

package com.episode6.myworkerapi.model.businessmember;

import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BusinessMemberMyList {
    @Schema(description = "시퀀스")
    private Long id;
    @Schema(description = "알바생 시퀀스")
    private Long memberId;
    @Schema(description = "알바생 이름")
    private String memberName;
    @Schema(description = "사업장 시퀀스")
    private Long businessId;
    @Schema(description = "사업장 이름")
    private String businessName;
    @Schema(description = "직책")
    private String position;

    private BusinessMemberMyList(Builder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.businessId = builder.businessId;
        this.businessName = builder.businessName;
        this.position = builder.position;
    }

    public static class Builder implements CommonModelBuilder<BusinessMemberMyList> {
        private final Long id;
        private final Long memberId;
        private final String memberName;
        private final Long businessId;
        private final String businessName;
        private final String position;

        public Builder(BusinessMember  businessMember) {
            this.id = businessMember.getId();
            this.memberId = businessMember.getMember().getId();
            this.memberName = businessMember.getMember().getName();
            this.businessId = businessMember.getBusiness().getId();
            this.businessName = businessMember.getBusiness().getBusinessName();
            this.position = businessMember.getPosition().getName();
        }
        @Override
        public BusinessMemberMyList build() {
            return new BusinessMemberMyList(this);
        }
    }
}

package com.episode6.myworkerapi.model.businessmember;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusinessMemberChangeWorkRequest {
    @NotNull
    @Schema(description = "근무 퇴직 전환")
    private Boolean isWork;
}

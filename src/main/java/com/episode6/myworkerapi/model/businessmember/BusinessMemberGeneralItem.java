package com.episode6.myworkerapi.model.businessmember;


import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Contract;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Schema
public class BusinessMemberGeneralItem {
    @Schema(description = "재직상태")
    private Boolean isWork;
    @Schema(description = "이름")
    private String memberName;
    @Schema(description = "전화번호")
    private String phoneNumber;
    @Schema(description = "근무지")
    private String workPlace;
    @Schema(description = "근무 시작일")
    private LocalDate dateWorkStart;
    @Schema(description = "근무 종료일")
    private LocalDate dateWorkEnd;

    private BusinessMemberGeneralItem(Builder builder) {
        this.isWork = builder.isWork;
        this.memberName = builder.memberName;
        this.phoneNumber = builder.phoneNumber;
        this.workPlace = builder.workPlace;
        this.dateWorkStart = builder.dateWorkStart;
        this.dateWorkEnd = builder.dateWorkEnd;
    }

    public static class Builder implements CommonModelBuilder<BusinessMemberGeneralItem> {

        private final Boolean isWork;
        private final String memberName;
        private final String phoneNumber;
        private final String workPlace;
        private final LocalDate dateWorkStart;
        private final LocalDate dateWorkEnd;

        public Builder(BusinessMember businessMember, Contract contract) {
            this.isWork = businessMember.getIsWork();
            this.memberName = businessMember.getMember().getName();
            this.phoneNumber = businessMember.getMember().getPhoneNumber();
            this.workPlace = businessMember.getBusiness().getReallyLocation();
            this.dateWorkStart = contract == null ? null : contract.getDateWorkStart();
            this.dateWorkEnd = contract == null ? null : contract.getDateWorkEnd();
        }


        @Override
        public BusinessMemberGeneralItem build() {
            return new BusinessMemberGeneralItem(this);
        }
    }
}

package com.episode6.myworkerapi.model.businessmember;


import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BusinessMemberDetailItem {
    private Long id;

    private Boolean isActivity;

    private String businessName;

    private String businessNumber;

    private String businessLocation;

    private String businessEmail;

    private Integer totalStaff;

    private BusinessMemberDetailItem(Builder builder) {
        this.id = builder.id;
        this.isActivity = builder.isActivity;
        this.businessName = builder.businessName;
        this.businessNumber = builder.businessNumber;
        this.businessLocation = builder.businessLocation;
        this.businessEmail = builder.businessEmail;
        this.totalStaff = builder.totalStaff;
    }

    public static class Builder implements CommonModelBuilder<BusinessMemberDetailItem> {
        private final Long id;

        private final Boolean isActivity;

        private final String businessName;

        private final String businessNumber;

        private final String businessLocation;

        private final String businessEmail;

        private final Integer totalStaff;


        public Builder(BusinessMember businessMember, int size) {
            this.id = businessMember.getBusiness().getId();
            this.isActivity = businessMember.getBusiness().getIsActivity();
            this.businessName = businessMember.getBusiness().getBusinessName();
            this.businessNumber = businessMember.getBusiness().getBusinessNumber();
            this.businessLocation = businessMember.getBusiness().getBusinessLocation();
            this.businessEmail = businessMember.getBusiness().getBusinessEmail();
            this.totalStaff = size;
        }

        @Override
        public BusinessMemberDetailItem build() {
            return new BusinessMemberDetailItem(this);
        }
    }
}

package com.episode6.myworkerapi.model.businessmember;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusinessMemberScheduleRequest {
    @NotNull
    @Schema(description = "근무 요일")
    private String weekSchedule;
    @NotNull
    @Schema(description = "근무 시작 시간")
    private String timeScheduleStart;
    @NotNull
    @Schema(description = "근무 퇴근 시간")
    private String timeScheduleEnd;
    @NotNull
    @Schema(description = "총 근무 시간")
    private String timeScheduleTotal;

}

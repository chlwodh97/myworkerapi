package com.episode6.myworkerapi.model.transition;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransitionRequest {
    @Schema(description = "인수인계 내용")
    private String content;
}

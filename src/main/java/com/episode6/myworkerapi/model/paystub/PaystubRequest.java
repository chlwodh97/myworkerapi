package com.episode6.myworkerapi.model.paystub;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class PaystubRequest {
    private Integer startDay;
    private Integer endDay;
}

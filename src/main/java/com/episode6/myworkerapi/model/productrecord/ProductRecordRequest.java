package com.episode6.myworkerapi.model.productrecord;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductRecordRequest {
    @Schema(description = "바꿀 현재 개수")
    private Short nowQuantity;
}

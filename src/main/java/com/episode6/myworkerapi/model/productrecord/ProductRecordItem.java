package com.episode6.myworkerapi.model.productrecord;

import com.episode6.myworkerapi.entity.ProductRecord;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductRecordItem {
    @Schema(description = "시퀀스")
    private Long id;
    @Schema(description = "프로덕트 시퀀스")
    private Long productId;
    @Schema(description = "상품 이름")
    private String productName;
    @Schema(description = "사업장 시퀀스")
    private Long businessId;
    @Schema(description = "사업장 이름")
    private String businessName;
    @Schema(description = "사업장 알바 시퀀스")
    private Long memberId;
    @Schema(description = "사업장 알바 이름")
    private String memberName;
    @Schema(description = "바꾼 현재 개수")
    private Short nowQuantity;
    @Schema(description = "등록 날짜")
    private String dateProductRecord;

    private ProductRecordItem(Builder builder) {
        this.id = builder.id;
        this.productId = builder.productId;
        this.productName = builder.productName;
        this.businessId = builder.businessId;
        this.businessName = builder.businessName;
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.nowQuantity = builder.nowQuantity;
        this.dateProductRecord = builder.dateProductRecord;
    }

    public static class Builder implements CommonModelBuilder<ProductRecordItem> {
        private final Long id;
        private final Long productId;
        private final String productName;
        private final Long businessId;
        private final String businessName;
        private final Long memberId;
        private final String memberName;
        private final Short nowQuantity;
        private final String dateProductRecord;

        public Builder(ProductRecord productRecord) {
            this.id = productRecord.getId();
            this.productId = productRecord.getProduct().getId();
            this.productName = productRecord.getProduct().getProductName();
            this.businessId = productRecord.getBusiness().getId();
            this.businessName = productRecord.getBusiness().getBusinessName();
            this.memberId = productRecord.getMember().getId();
            this.memberName = productRecord.getMember().getName();
            this.nowQuantity = productRecord.getNowQuantity();
            this.dateProductRecord = productRecord.getDateProductRecord().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));
        }

        @Override
        public ProductRecordItem build() {
            return new ProductRecordItem(this);
        }
    }
}

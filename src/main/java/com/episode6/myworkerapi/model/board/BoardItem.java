package com.episode6.myworkerapi.model.board;

import com.episode6.myworkerapi.entity.Board;
import com.episode6.myworkerapi.enums.Category;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardItem {
    @Schema(description = "아이디")
    private Long id;

    @Schema(description = "카테고리")
    private Category category;

    @Schema(description = "제목")
    private String title;

    @Schema(description = "본문")
    private String content;

    @Schema(description = "이미지")
    private String boardImgUrl;

    @Schema(description = "작성일")
    private String dateBoard;

    @Schema(description = "글쓴이")
    private Long memberId;

    private BoardItem(Builder builder) {
        this.id = builder.id;
        this.category = builder.category;
        this.title = builder.title;
        this.content = builder.content;
        this.boardImgUrl = builder.boardImgUrl;
        this.dateBoard = builder.dateBoard;
        this.memberId = builder.memberId;
    }

    public static class Builder implements CommonModelBuilder<BoardItem> {
        private final Long id;
        private final Category category;
        private final String title;
        private final String content;
        private final String boardImgUrl;
        private final String dateBoard;
        private final Long memberId;

        public Builder(Board board) {
            this.id = board.getId();
            this.category = board.getCategory();
            this.title = board.getTitle();
            this.content = board.getContent();
            this.boardImgUrl = board.getBoardImgUrl();
            this.dateBoard = board.getDateBoard().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));
            this.memberId = board.getMember().getId();
        }
        @Override
        public BoardItem build() {
            return new BoardItem(this);
        }
    }
}

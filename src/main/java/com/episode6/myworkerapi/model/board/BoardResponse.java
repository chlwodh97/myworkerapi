package com.episode6.myworkerapi.model.board;

import com.episode6.myworkerapi.entity.Board;
import com.episode6.myworkerapi.entity.Comment;
import com.episode6.myworkerapi.enums.Category;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.comment.CommentDetailItem;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.time.format.DateTimeFormatter;
import java.util.List;

@Getter
@Setter
@Schema
public class BoardResponse {
    @Schema(description = "아이디")
    private Long id;

    @Schema(description = "카테고리")
    private Category category;

    @Schema(description = "제목")
    private String title;

    @Schema(description = "본문")
    private String content;

    @Schema(description = "이미지")
    private String boardImgUrl;

    @Schema(description = "작성일")
    private String dateBoard;

    @Schema(description = "글쓴이")
    private Long memberId;

    @Schema(description = "댓글")
    private List<CommentDetailItem> comments;

    private BoardResponse(Builder builder) {
        this.id = builder.id;
        this.category = builder.category;
        this.title = builder.title;
        this.content = builder.content;
        this.boardImgUrl = builder.boardImgUrl;
        this.dateBoard = builder.dateBoard;
        this.memberId = builder.memberId;

        this.comments = builder.comments;

    }

    public static class Builder implements CommonModelBuilder<BoardResponse> {
        private final Long id;
        private final Category category;
        private final String title;
        private final String content;
        private final String boardImgUrl;
        private final String dateBoard;
        private final Long memberId;

        private List<CommentDetailItem> comments;

        public Builder(Board board, List<CommentDetailItem> comments) {
            this.id = board.getId();
            this.category = board.getCategory();
            this.title = board.getTitle();
            this.content = board.getContent();
            this.boardImgUrl = board.getBoardImgUrl();
            this.dateBoard = board.getDateBoard().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));
            this.memberId = board.getMember().getId();

            this.comments = comments;
        }

        public Builder setComment(Comment comment) {
            this.comments = comment == null ? null : comments;
            return this;
        }

        @Override
        public BoardResponse build() {
            return new BoardResponse(this);
        }
    }
}
package com.episode6.myworkerapi.model.contract;

import com.episode6.myworkerapi.entity.Contract;
import com.episode6.myworkerapi.enums.PayType;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;

@Getter
@Schema
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ContractResponse {
    private Long id;
    @Schema(description = "비즈니스 멤버 id")
    private Long businessMemberId;
    @Schema(description = "알바생 이름")
    private String businessMemberName;
    @Schema(description = "직급")
    private String businessMemberPosition;
    @Schema(description = "근로계약일자")
    private String dateContract;
    @Schema(description = "급여책정방식")
    private String payType;
    @Schema(description = "급여금액")
    private Double payPrice;
    @Schema(description = "근로시작일")
    private String dateWorkStart;
    @Schema(description = "근로종료일")
    private String dateWorkEnd;
    @Schema(description = "휴식시간")
    private Short restTime;
    @Schema(description = "세금공제")
    private String insurance;
    @Schema(description = "근무요일")
    private String workDay;
    @Schema(description = "일 근무시간")
    private Short workTime;
    @Schema(description = "주 근무시간")
    private Short weekWorkTime;
    @Schema(description = "주휴수당여부")
    private String isWeekPay;
    @Schema(description = "식대")
    private Double mealPay;
    @Schema(description = "임금 지급일")
    private String wagePayment;
    @Schema(description = "임금 계좌번호")
    private String wageAccountNumber;
    @Schema(description = "근로계약서사본")
    private String contractCopyImgUrl;

    private ContractResponse(Builder builder){
        this.id = builder.id;
        this.businessMemberId = builder.businessMemberId;
        this.businessMemberName = builder.businessMemberName;
        this.businessMemberPosition = builder.businessMemberPosition;
        this.dateContract = builder.dateContract;
        this.payType = builder.payType;
        this.payPrice = builder.payPrice;
        this.dateWorkStart = builder.dateWorkStart;
        this.dateWorkEnd = builder.dateWorkEnd;
        this.restTime = builder.restTime;
        this.insurance = builder.insurance;
        this.workDay = builder.workDay;
        this.workTime = builder.workTime;
        this.weekWorkTime = builder.weekWorkTime;
        this.isWeekPay = builder.isWeekPay;
        this.mealPay = builder.mealPay;
        this.wagePayment = builder.wagePayment;
        this.wageAccountNumber = builder.wageAccountNumber;
        this.contractCopyImgUrl = builder.contractCopyImgUrl;
    }

    public static class Builder implements CommonModelBuilder<ContractResponse> {
        private final Long id;
        private final Long businessMemberId;
        private final String businessMemberName;
        private final String businessMemberPosition;
        private final String dateContract;
        private final String payType;
        private final Double payPrice;
        private final String dateWorkStart;
        private final String dateWorkEnd;
        private final Short restTime;
        private final String insurance;
        private final String workDay;
        private final Short workTime;
        private final Short weekWorkTime;
        private final String isWeekPay;
        private final Double mealPay;
        private final String wagePayment;
        private final String wageAccountNumber;
        private final String contractCopyImgUrl;

        public Builder(Contract contract) {

            this.id = contract.getId();
            this.businessMemberId = contract.getBusinessMember().getId();
            this.businessMemberName = contract.getBusinessMember().getMember().getName();
            this.businessMemberPosition = contract.getBusinessMember().getPosition().getName();
            this.dateContract = contract.getDateContract().format(DateTimeFormatter.ofPattern("yyyy.MM.dd"));
            this.payType = contract.getPayType().getName();
            this.payPrice = contract.getPayPrice();
            this.dateWorkStart = contract.getDateWorkStart().format(DateTimeFormatter.ofPattern("yyyy.MM.dd"));
            this.dateWorkEnd = contract.getDateWorkEnd().format(DateTimeFormatter.ofPattern("yyyy.MM.dd"));
            this.restTime = contract.getRestTime();
            this.insurance = contract.getInsurance().getName();
            this.workDay = contract.getWorkDay();
            this.workTime = contract.getWorkTime();
            this.weekWorkTime = contract.getWeekWorkTime();
            this.isWeekPay = contract.getIsWeekPay() ? "Y" : "N";
            this.mealPay = contract.getMealPay();
            this.wagePayment = contract.getWagePayment();
            this.wageAccountNumber = contract.getWageAccountNumber();
            this.contractCopyImgUrl = contract.getContractCopyImgUrl();
        }
        @Override
        public ContractResponse build() {
            return new ContractResponse(this);
        }
    }
}

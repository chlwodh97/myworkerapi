package com.episode6.myworkerapi.model.contract;


import com.episode6.myworkerapi.enums.Insurance;
import com.episode6.myworkerapi.enums.PayType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Schema
public class ContractRequest {
    @Schema(description = "근로 계약 일자")
    private LocalDate dateContract;
    @Schema(description = "급여 책정 방식")
    private PayType payType;
    @Schema(description = "급여 금액")
    private Double payPrice;
    @Schema(description = "근로시작일")
    private LocalDate dateWorkStart;
    @Schema(description = "근로종료일")
    private LocalDate dateWorkEnd;
    @Schema(description = "휴게시간")
    private Short restTime;
    @Schema(description = "세금공제")
    private Insurance insurance;
    @Schema(description = "근무요일")
    private String workDay;
    @Schema(description = "일 근무시간")
    private Short workTime;
    @Schema(description = "주 근무시간")
    private Short weekWorkTime;
    @Schema(description = "주휴수당 여부")
    private Boolean isWeekPay;
    @Schema(description = "식대")
    private Double mealPay;
    @Schema(description = "임금 지급일")
    private String wagePayment;
    @Schema(description = "임금 계좌번호")
    private String wageAccountNumber;
    @Schema(description = "근로계약서 사본")
    private String contractCopyImgUrl;

}

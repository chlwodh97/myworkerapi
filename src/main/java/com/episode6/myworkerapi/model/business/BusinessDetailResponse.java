package com.episode6.myworkerapi.model.business;


import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BusinessDetailResponse {
    private Long id;
    @Schema(description = "사업장 영업여부")
    private Boolean isActivity;
    @Schema(description = "상호명")
    private String businessName;
    @Schema(description = "사업자 번호")
    private String businessNumber;
    @Schema(description = "소재지")
    private String businessLocation;
    @Schema(description = "이메일")
    private String businessEmail;
    @Schema(description = "총 직원 수")
    private Integer totalStaff;




    private BusinessDetailResponse(Builder builder) {
        this.id = builder.id;
        this.isActivity = builder.isActivity;
        this.businessName = builder.businessName;
        this.businessNumber = builder.businessNumber;
        this.businessLocation = builder.businessLocation;
        this.businessEmail = builder.businessEmail;
        this.totalStaff = builder.totalStaff;

    }


    public static class Builder implements CommonModelBuilder<BusinessDetailResponse> {
        private final Long id;

        private final Boolean isActivity;

        private final String businessName;

        private final String businessNumber;

        private final String businessLocation;

        private final String businessEmail;

        private final Integer totalStaff;



        public Builder(Business business, int size) {
            this.id = business.getId();
            this.isActivity = business.getIsActivity();
            this.businessName = business.getBusinessName();
            this.businessNumber = business.getBusinessNumber();
            this.businessLocation  = business.getBusinessLocation();
            this.businessEmail = business.getBusinessEmail();
            this.totalStaff = size;
            // 사업장 회원 수 비즈니스 멤버에서 멤버를 찾아서 사이즈 잰다 -> 비즈니스멤버 레포지토리에서 비즈니스를 찾아서 멤버 수
        }

        @Override
        public BusinessDetailResponse build() {
            return new BusinessDetailResponse(this);
        }
    }
}

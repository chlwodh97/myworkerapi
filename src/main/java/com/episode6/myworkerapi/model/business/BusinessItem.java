package com.episode6.myworkerapi.model.business;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BusinessItem {
    private Long id;
    @Schema(description = "멤버Id")
    private Long memberId;
    @Schema(description = "상호명")
    private String businessName;
    @Schema(description = "사업자 번호")
    private String businessNumber;
    @Schema(description = "대표자명")
    private String ownerName;
    @Schema(description = "사업자등록증 이미지")
    private String businessImgUrl;
    @Schema(description = "업종")
    private String businessType;
    @Schema(description = "이메일")
    private String businessEmail;
    @Schema(description = "전화번호")
    private String businessPhoneNumber;
    @Schema(description = "사업장 영업여부")
    private String isActivity;
    @Schema(description = "등록 일시")
    private String dateJoinBusiness;
    @Schema(description = "승인 여부")
    private String isApprovalBusiness;
    @Schema(description = "사업장 등록 반려 사유")
    private String refuseReason;
    @Schema(description = "사업장 수정 요청 반려 사유")
    private String refuseFix;

    private BusinessItem(Builder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.businessName = builder.businessName;
        this.businessNumber = builder.businessNumber;
        this.ownerName = builder.ownerName;
        this.businessImgUrl = builder.businessImgUrl;
        this.businessType = builder.businessType;
        this.businessEmail = builder.businessEmail;
        this.businessPhoneNumber = builder.businessPhoneNumber;
        this.isActivity = builder.isActivity;
        this.dateJoinBusiness = builder.dateJoinBusiness;
        this.isApprovalBusiness = builder.isApprovalBusiness;
        this.refuseReason = builder.refuseReason;
        this.refuseFix = builder.refuseFix;
    }

    public static class Builder implements CommonModelBuilder<BusinessItem> {

        private final Long id;
        private final Long memberId;
        private final String businessName;
        private final String businessNumber;
        private final String ownerName;
        private final String businessImgUrl;
        private final String businessType;
        private final String businessEmail;
        private final String businessPhoneNumber;
        private final String isActivity;
        private final String dateJoinBusiness;
        private final String isApprovalBusiness;
        private final String refuseReason;
        private final String refuseFix;


        public Builder(Business business) {
            this.id = business.getId();
            this.memberId = business.getMember().getId();
            this.businessName = business.getBusinessName();
            this.businessNumber = business.getBusinessNumber();
            this.ownerName = business.getOwnerName();
            this.businessImgUrl = business.getBusinessImgUrl();
            this.businessType = business.getBusinessType();
            this.businessEmail = business.getBusinessEmail();
            this.businessPhoneNumber = business.getBusinessPhoneNumber();
            this.isActivity = business.getIsActivity() ? "정상 영업중" : "폐업";
            this.dateJoinBusiness = business.getDateJoinBusiness().format(DateTimeFormatter.ofPattern("yyyy.MM.dd"));
            this.isApprovalBusiness = business.getIsApprovalBusiness() ? "승인 완료" : "승인 대기중";
            this.refuseReason = business.getRefuseReason();
            this.refuseFix = business.getRefuseFix();
        }
        @Override
        public BusinessItem build() {
            return new BusinessItem(this);
        }
    }
}

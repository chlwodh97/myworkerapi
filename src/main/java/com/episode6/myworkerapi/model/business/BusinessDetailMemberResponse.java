package com.episode6.myworkerapi.model.business;


import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.businessmember.BusinessMemberGeneralItem;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@Schema
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BusinessDetailMemberResponse {
    @Schema(description = "사업장 id")
    private Long businessId;
    @Schema(description = "상호명")
    private String businessName;
    @Schema(description = "대표자명")
    private String ownerName;
    @Schema(description = "영업여부")
    private Boolean isActivity;
    @Schema(description = "승인여부")
    private Boolean isApprovalBusiness;
    @Schema(description = "전화번호")
    private String businessPhoneNumber;

    private List<BusinessMemberGeneralItem> memberList;


    private BusinessDetailMemberResponse(Builder builder) {
        this.businessId = builder.businessId;
        this.businessName = builder.businessName;
        this.ownerName = builder.ownerName;
        this.isActivity = builder.isActivity;
        this.isApprovalBusiness = builder.isApprovalBusiness;
        this.businessPhoneNumber = builder.businessPhoneNumber;
        this.memberList = builder.memberList;
    }

    public static class Builder implements CommonModelBuilder<BusinessDetailMemberResponse> {

        private final Long businessId;
        private final String businessName;
        private final String ownerName;
        private final Boolean isActivity;
        private final Boolean isApprovalBusiness;
        private final String businessPhoneNumber;

        private List<BusinessMemberGeneralItem> memberList;

        public Builder(Business business, List<BusinessMemberGeneralItem> memberList) {
            this.businessId = business.getId();
            this.businessName = business.getBusinessName();
            this.ownerName = business.getOwnerName();
            this.isActivity = business.getIsActivity();
            this.isApprovalBusiness = business.getIsApprovalBusiness();
            this.businessPhoneNumber = business.getBusinessPhoneNumber();

            this.memberList = memberList;
        }

        public Builder setMemberList(BusinessMember businessMember) {
            this.memberList = businessMember == null ? null : memberList;
            return this;
        }

        @Override
        public BusinessDetailMemberResponse build() {
            return new BusinessDetailMemberResponse(this);
        }
    }
}

package com.episode6.myworkerapi.model.statistics;

import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Schedule;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.businessmember.BusinessMemberStartDayItem;
import com.episode6.myworkerapi.model.schedule.ScheduleLatenessName;
import com.episode6.myworkerapi.model.schedule.ScheduleOutLatenessName;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StatisticsScheduleResponse {
    private List<BusinessMemberStartDayItem> businessMemberStartDayItem;
    private List<ScheduleLatenessName> scheduleLatenessNames;
    private List<ScheduleOutLatenessName> scheduleOutLatenessNames;

    private StatisticsScheduleResponse(Builder builder) {
        this.businessMemberStartDayItem = builder.businessMemberStartDayItem;
        this.scheduleLatenessNames = builder.scheduleLatenessNames;
        this.scheduleOutLatenessNames = builder.scheduleOutLatenessNames;
    }

    public static class Builder implements CommonModelBuilder<StatisticsScheduleResponse> {
        private List<BusinessMemberStartDayItem> businessMemberStartDayItem;
        private List<ScheduleLatenessName> scheduleLatenessNames;
        private List<ScheduleOutLatenessName> scheduleOutLatenessNames;

        public Builder(List<BusinessMemberStartDayItem> businessMemberStartDayItem, List<ScheduleLatenessName> scheduleLatenessNames, List<ScheduleOutLatenessName> scheduleOutLatenessNames) {
            this.businessMemberStartDayItem = businessMemberStartDayItem;
            this.scheduleLatenessNames = scheduleLatenessNames;
            this.scheduleOutLatenessNames = scheduleOutLatenessNames;
        }

        public Builder dayItem(BusinessMember businessMember) {
            this.businessMemberStartDayItem = businessMember == null ? null : businessMemberStartDayItem;
            return this;
        }

        public Builder latenessNames(Schedule schedule) {
            this.scheduleLatenessNames = schedule == null ? null : scheduleLatenessNames;
            return this;
        }

        public Builder outLatenessNames(Schedule schedule) {
            this.scheduleOutLatenessNames = schedule == null ? null : scheduleOutLatenessNames;
            return this;
        }

        @Override
        public StatisticsScheduleResponse build() {
            return new StatisticsScheduleResponse(this);
        }
    }
}

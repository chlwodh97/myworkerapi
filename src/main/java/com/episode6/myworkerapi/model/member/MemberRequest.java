package com.episode6.myworkerapi.model.member;

import com.episode6.myworkerapi.enums.MemberType;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

@Schema(description = "회원 가입")
@Getter
@Setter
public class MemberRequest {
    @NotNull
    @Schema(description = "회원 타입")
    private MemberType memberType;

    @NotNull
    @Length(min = 2, max = 20)
    @Schema(description = "실명", minLength = 2, maxLength = 20)
    private String name;

    @NotNull
    @Length(min = 5, max = 40)
    @Schema(description = "유저아이디", minLength = 5, maxLength = 40)
    private String username;

    @NotNull
    @Length(min = 5, max = 20)
    @Schema(description = "비밀번호", minLength = 5, maxLength = 20)
    private String password;

    @NotNull
    @Length(min = 5, max = 20)
    @Schema(description = "비밀번호확인", minLength = 5, maxLength = 20)
    private String passwordRe;

    @NotNull
    @Schema(description = "성별")
    private String isMan;

    @NotNull
    @Schema(description = "생년월일")
    private LocalDate dateBirth;

    @NotNull
    @Length(min = 5, max = 13)
    @Schema(description = "폰번호", minLength = 10, maxLength = 13)
    private String phoneNumber;

    @Length(max = 40)
    @Schema(description = "주소지", maxLength = 40, nullable = true)
    private String address;
}

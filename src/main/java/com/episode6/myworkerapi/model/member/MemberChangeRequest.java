package com.episode6.myworkerapi.model.member;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class MemberChangeRequest {
    @NotNull
    @Length(min = 2, max = 20)
    @Schema(description = "실명", minLength = 2, maxLength = 20)
    private String name;
    @NotNull
    @Length(min = 5, max = 13)
    @Schema(description = "폰번호", minLength = 10, maxLength = 13)
    private String phoneNumber;
    @Length(max = 40)
    @Schema(description = "주소지", maxLength = 40)
    private String address;
}

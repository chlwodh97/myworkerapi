package com.episode6.myworkerapi.model.member;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class MemberPasswordCheckRequest {
    @NotNull
    @Length(min = 5, max = 20)
    @Schema(description = "비밀번호")
    private String password;

    @NotNull
    @Length(min = 5, max = 20)
    @Schema(description = "비밀번호 확인")
    private String passwordRe;
}

package com.episode6.myworkerapi.model.member;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberResponse {
    @Schema(description = "시퀀스")
    private Long id;
    @Schema(description = "회원 상태")
    private String memberState;
    @Schema(description = "회원 타입")
    private String memberType;
    @Schema(description = "실명")
    private String name;
    @Schema(description = "유저아이디")
    private String username;
    @Schema(description = "성별")
    private String isMan;
    @Schema(description = "생년월일")
    private LocalDate dateBirth;
    @Schema(description = "폰번호")
    private String phoneNumber;
    @Schema(description = "주소지")
    private String address;
    @Schema(description = "회원가입한 날짜시간")
    private String dateMember;

    private MemberResponse(Builder builder) {
        this.id = builder.id;
        this.memberState = builder.memberState;
        this.memberType = builder.memberType;
        this.name = builder.name;
        this.username = builder.username;
        this.isMan = builder.isMan;
        this.dateBirth = builder.dateBirth;
        this.phoneNumber = builder.phoneNumber;
        this.address = builder.address;
        this.dateMember = builder.dateMember;
    }

    public static class Builder implements CommonModelBuilder<MemberResponse> {
        private final Long id;
        private final String memberState;
        private final String memberType;
        private final String name;
        private final String username;
        private final String isMan;
        private final LocalDate dateBirth;
        private final String phoneNumber;
        private final String address;
        private final String dateMember;

        public Builder(Member member) {
            this.id = member.getId();
            this.memberState = member.getMemberState().getName();
            this.memberType = member.getMemberType().getName();
            this.name = member.getName();
            this.username = member.getUsername();
            this.isMan = member.getIsMan() ? "남자" : "여자";
            this.dateBirth = member.getDateBirth();
            this.phoneNumber = member.getPhoneNumber();
            this.address = member.getAddress();
            this.dateMember = member.getDateMember().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));
        }
        @Override
        public MemberResponse build() {
            return new MemberResponse(this);
        }
    }
}

package com.episode6.myworkerapi.model.member;


import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.enums.MemberState;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.business.BusinessDetailResponse;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;



@Getter
@Schema
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberBossDetailResponse {

    private String MemberName;

    private MemberState MemberState;

    private LocalDate DateBirth;

    private String PhoneNumber;

    private String UserName;

    private String DateMember;

    private BusinessDetailResponse businessDetailResponse;

    private MemberBossDetailResponse(Builder builder) {
        this.MemberName = builder.MemberName;
        this.MemberState = builder.MemberState;
        this.DateBirth = builder.DateBirth;
        this.PhoneNumber = builder.PhoneNumber;
        this.UserName = builder.UserName;
        this.DateMember = builder.DateMember;

        this.businessDetailResponse = builder.businessDetailResponse;
    }


    public static class Builder implements CommonModelBuilder<MemberBossDetailResponse> {

        private final String MemberName;

        private final MemberState MemberState;

        private final LocalDate DateBirth;

        private final String PhoneNumber;

        private final String UserName;

        private final String DateMember;

        private BusinessDetailResponse businessDetailResponse;

        public Builder(Member member) {
            this.MemberName = member.getName();
            this.MemberState = member.getMemberState();
            this.DateBirth = member.getDateBirth();
            this.PhoneNumber = member.getPhoneNumber();
            this.UserName = member.getUsername();
            this.DateMember = member.getDateMember().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));
        }

        public Builder BusinessDetailResponse(Business business, int size) {
            this.businessDetailResponse = business == null ? null : new BusinessDetailResponse.Builder(business, size).build();
            return this;
        }

        @Override
        public MemberBossDetailResponse build() {
            return new MemberBossDetailResponse(this);
        }
    }
}

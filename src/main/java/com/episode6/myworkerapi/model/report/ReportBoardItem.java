package com.episode6.myworkerapi.model.report;

import com.episode6.myworkerapi.entity.ReportBoard;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
@Schema
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReportBoardItem {
    @Schema(description = "게시글 id")
    private Long boardId;
    @Schema(description = "멤버 id")
    private Long memberId;
    @Schema(description = "신고내용")
    private String content;
    @Schema(description = "신고 날짜")
    private String dateReport;


    private ReportBoardItem(Builder builder) {
        this.boardId = builder.boardId;
        this.memberId = builder.memberId;
        this.content = builder.content;
        this.dateReport = builder.dateReport;
    }

    public static class Builder implements CommonModelBuilder<ReportBoardItem> {
        private final Long boardId;
        private final Long memberId;
        private final String content;
        private final String dateReport;

        public Builder(ReportBoard reportBoard) {
            this.boardId = reportBoard.getId();
            this.memberId = reportBoard.getMember().getId();
            this.content = reportBoard.getContent();
            this.dateReport = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));
        }
        @Override
        public ReportBoardItem build() {
            return new ReportBoardItem(this);
        }
    }
}

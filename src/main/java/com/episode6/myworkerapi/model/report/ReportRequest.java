package com.episode6.myworkerapi.model.report;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportRequest {
    @Schema(description = "신고 내용")
    private String content;

}

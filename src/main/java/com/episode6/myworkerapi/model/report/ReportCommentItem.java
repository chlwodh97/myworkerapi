package com.episode6.myworkerapi.model.report;

import com.episode6.myworkerapi.entity.ReportComment;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
@Schema
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReportCommentItem {
    @Schema(description = "댓글 id")
    private Long commentId;
    @Schema(description = "멤버 id")
    private Long memberId;
    @Schema(description = "신고 내용")
    private String content;
    @Schema(description = "신고 날짜")
    private String dateReport;


    private ReportCommentItem(Builder builder) {
        this.commentId = builder.commentId;
        this.memberId = builder.memberId;
        this.content = builder.content;
        this.dateReport = builder.dateReport;
    }

    public static class Builder implements CommonModelBuilder<ReportCommentItem> {
        private final Long commentId;
        private final Long memberId;
        private final String content;
        private final String dateReport;


        public Builder(ReportComment reportComment) {
            this.commentId = reportComment.getId();
            this.memberId = reportComment.getMember().getId();
            this.content = reportComment.getContent();
            this.dateReport = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));
        }
        @Override
        public ReportCommentItem build() {
            return new ReportCommentItem(this);
        }
    }
}



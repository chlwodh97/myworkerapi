package com.episode6.myworkerapi.model.report;


import com.episode6.myworkerapi.entity.ReportComment;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;


@Schema
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReportCommentDetailItem {
    @Schema(description = "시퀀스")
    private Long id;
    @Schema(description = "신고 유저 이름")
    private String reportUserName;
    @Schema(description = "신고 내용")
    private String reportContent;
    @Schema(description = "신고 날짜")
    private String dateReport;
    @Schema(description = "댓글 작성자 이름")
    private String commentMemberUserName;
    @Schema(description = "댓글 작성일")
    private String dateComment;
    @Schema(description = "신고 댓글 내용")
    private String commentContent;

    private ReportCommentDetailItem(Builder builder) {
        this.id = builder.id;
        this.reportUserName = builder.reportUserName;
        this.reportContent = builder.reportContent;
        this.dateReport = builder.dateReport;
        this.commentMemberUserName = builder.commentMemberUserName;
        this.dateComment = builder.dateComment;
        this.commentContent = builder.commentContent;

    }


    public static class Builder implements CommonModelBuilder<ReportCommentDetailItem> {
        private final Long id;
        private final String reportUserName;
        private final String reportContent;
        private final String dateReport;
        private final String commentMemberUserName;
        private final String dateComment;
        private final String commentContent;


        public Builder(ReportComment reportComment) {
            this.id = reportComment.getMember().getId();
            this.reportUserName = reportComment.getMember().getName();
            this.reportContent = reportComment.getContent();
            this.dateReport = reportComment.getDateReport().format(DateTimeFormatter.ofPattern("yyyy.MM.dd"));
            this.commentMemberUserName = reportComment.getComment().getMember().getName();
            this.dateComment = reportComment.getComment().getDateContent().format(DateTimeFormatter.ofPattern("yyyy.MM.dd"));
            this.commentContent = reportComment.getComment().getContent();

        }

        @Override
        public ReportCommentDetailItem build() {
            return new ReportCommentDetailItem(this);
        }
    }
}

package com.episode6.myworkerapi.model.notice;

import com.episode6.myworkerapi.entity.Notice;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Schema
public class NoticeDashBoardItem {
    private Long id;
    @Schema(description = "공지 제목")
    private String noticeTitle;
    @Schema(description = "작성 시간")
    private String noticeTime;


    private NoticeDashBoardItem(Builder builder) {
        this.noticeTitle = builder.noticeTitle;
        this.noticeTime = builder.noticeTime;
        this.id = builder.id;
    }

    public static class Builder implements CommonModelBuilder<NoticeDashBoardItem> {
        private final Long id;
        private final String noticeTitle;
        private final String noticeTime;


        public Builder(Notice notice) {
            this.noticeTitle = notice.getTitle();
            this.noticeTime = notice.getDateNotice().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));
            this.id = notice.getId();
        }


        @Override
        public NoticeDashBoardItem build() {
            return new NoticeDashBoardItem(this);
        }
    }
}

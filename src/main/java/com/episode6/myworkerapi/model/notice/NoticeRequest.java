package com.episode6.myworkerapi.model.notice;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class NoticeRequest {
    @NotNull
    @Length(min = 2, max = 30)
    @Schema(description = "공지 제목", minLength = 2, maxLength = 30)
    private String title;
    @NotNull
    @Schema(description = "공지 내용")
    private String content;
}

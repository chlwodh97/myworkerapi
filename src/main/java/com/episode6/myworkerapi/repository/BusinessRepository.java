package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;

public interface BusinessRepository extends JpaRepository<Business, Long> {

    /**
     * 사업장 최신순
     */
    Page<Business> findByOrderByIdDesc(Pageable pageable);

    Business findBusinessByMember(Member member);

    Business findByMember(Member member);

    Long countByDateJoinBusinessBetween(LocalDateTime start, LocalDateTime end);
}

package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.AskAnswer;
import com.episode6.myworkerapi.entity.MemberAsk;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AskAnswerRepository extends JpaRepository<AskAnswer, Long> {
    Page<AskAnswer> findAllByOrderByIdDesc(Pageable pageable);

    AskAnswer findByMemberAsk(MemberAsk memberAsk);

    Optional<AskAnswer> findByMemberAskOrderById(MemberAsk memberAsk);

    List<AskAnswer> findAllByMemberAsk(MemberAsk memberAsk);
}

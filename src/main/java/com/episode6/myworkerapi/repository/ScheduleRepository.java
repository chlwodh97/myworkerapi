package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.entity.Schedule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
    Optional<Schedule> findByBusinessMemberAndDateWork(BusinessMember businessMember, LocalDate localDate);

    Schedule findByDateWorkAndBusinessMember(LocalDate localDate, BusinessMember businessMember);

    Page<Schedule> findAllByOrderByIdDesc(Pageable pageable);
    Page<Schedule> findAllByBusinessMember_Member(Member member, Pageable pageable);
    Page<Schedule> findAllByBusinessMember_BusinessOrderByIdDesc(Business business, Pageable pageable);
    Page<Schedule> findAllByBusinessMemberOrderByIdDesc(BusinessMember businessMember, Pageable pageable);

    Schedule findByBusinessMember(BusinessMember businessMember);

    List<Schedule> findAllByDateWorkBetween(LocalDate start, LocalDate end);

    List<Schedule> findAllByBusinessMember_BusinessAndDateWorkBetween(Business business, LocalDate start, LocalDate end);

    List<Schedule> findAllByBusinessMember_BusinessAndBusinessMember_MemberAndDateWorkBetween(Business business, Member member, LocalDate start, LocalDate end);

    List<Schedule> findAllByBusinessMember_BusinessAndDateWork(Business business, LocalDate today);

    List<Schedule> findAllByBusinessMember_BusinessAndDateWorkAndIsLateness(Business business, LocalDate today, boolean isLateness);

    List<Schedule> findAllByBusinessMember_BusinessAndBusinessMember_MemberAndDateWork(Business business, Member member, LocalDate today);
}

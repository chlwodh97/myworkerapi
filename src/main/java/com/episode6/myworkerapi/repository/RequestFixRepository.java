package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.RequestFix;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestFixRepository extends JpaRepository<RequestFix, Long> {

    Page<RequestFix> findAllByOrderByIdDesc(Pageable pageable);


}

package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    Page<Product> findAllByOrderByIdDesc(Pageable pageable);
    Page<Product> findAllByBusinessOrderByIdDesc(Business business, Pageable pageable);

    List<Product> findByBusiness(Business business);
}

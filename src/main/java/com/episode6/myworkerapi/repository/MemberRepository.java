package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.enums.MemberType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    long countByUsername(String username);

    Page<Member> findAllByOrderByIdDesc(Pageable pageable);

    Page<Member> findAllByMemberTypeOrderByIdDesc(Pageable pageable, MemberType memberType);

    Member findByMemberTypeAndUsername(MemberType memberType, String username);

    Optional<Member> findByUsername(String username);

    Member findByPhoneNumber(String phoneNumber);

    Long countByDateMemberBetween(LocalDateTime start, LocalDateTime end);

}

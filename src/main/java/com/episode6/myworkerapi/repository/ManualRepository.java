package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Manual;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManualRepository extends JpaRepository<Manual, Long> {

    Page<Manual> findAllByOrderByIdDesc(Pageable pageable);


    Page<Manual> findByBusinessOrderByIdDesc(Pageable pageable, Business business);
}

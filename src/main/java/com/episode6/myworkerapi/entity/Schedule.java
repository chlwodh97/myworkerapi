package com.episode6.myworkerapi.entity;


import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.schedule.ScheduleEtcRequest;
import com.episode6.myworkerapi.model.schedule.ScheduleRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Schedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "businessMemberId" , nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private BusinessMember businessMember;

    @Column(nullable = false)
    private LocalDate dateWork;

    @Column(nullable = false)
    private LocalTime startWork;

    /**
     * 위도 , 경도에 글자수 제한 ..?
     */
    @Column(nullable = false)
    private Double startLatitude;

    @Column(nullable = false)
    private Double startLongitude;

    private LocalTime endWork;

    private Double endLatitude;

    private Double endLongitude;

    private Integer totalWorkTime;

    private Boolean isLateness;

    private Boolean isOverTime;

    @Column(length = 50)
    private String etc;

    public void putEtc(ScheduleEtcRequest request) {
        this.etc = request.getEtc();
    }

    public void putEndWork(ScheduleRequest request, int totalWorkTime, boolean isOverTime) {
        this.endWork = LocalTime.now();
        this.endLatitude = request.getLatitude();
        this.endLongitude = request.getLongitude();
        this.totalWorkTime = totalWorkTime;
        this.isOverTime = isOverTime;
    }

    private Schedule(Builder builder) {
        this.businessMember = builder.businessMember;
        this.dateWork = builder.dateWork;
        this.startWork = builder.startWork;
        this.startLatitude = builder.startLatitude;
        this.startLongitude = builder.startLongitude;
        this.etc = builder.etc;
        this.isLateness = builder.isLateness;
        this.isOverTime = builder.isOverTime;
    }

    public static class Builder implements CommonModelBuilder<Schedule> {
        private final BusinessMember businessMember;
        private final LocalDate dateWork;
        private final LocalTime startWork;
        private final Double startLatitude;
        private final Double startLongitude;
        private final String etc;
        private final Boolean isLateness;
        private final Boolean isOverTime;

        public Builder(BusinessMember businessMember, ScheduleRequest request, boolean isLateness) {
            this.businessMember = businessMember;
            this.dateWork = LocalDate.now();
            this.startWork = LocalTime.now();
            this.startLatitude = request.getLatitude();
            this.startLongitude = request.getLongitude();
            this.etc = null;
            this.isLateness = isLateness;
            this.isOverTime = false;
        }

        @Override
        public Schedule build() {
            return new Schedule(this);
        }
    }
}

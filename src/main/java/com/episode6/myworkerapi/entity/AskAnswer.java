package com.episode6.myworkerapi.entity;


import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.askanswer.AskAnswerRequest;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Schema(description = "sdlfjaldfkj")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AskAnswer {
    @Id
    @Schema(description = "시퀀스")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @Schema(description = "관리자아이디")
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberAskId", nullable = false)
    private MemberAsk memberAsk;

    @Column(nullable = false, length = 30)
    private String answerTitle;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String answerContent;

    private String answerImgUrl;

    @Column(nullable = false)
    private LocalDateTime dateAnswer;

    public void putAskAnswer(AskAnswerRequest request) {
        this.answerTitle = request.getAnswerTitle();
        this.answerContent = request.getAnswerContent();
    }

    private AskAnswer(Builder builder) {
        this.member = builder.member;
        this.memberAsk = builder.memberAsk;
        this.answerTitle = builder.answerTitle;
        this.answerContent = builder.answerContent;
        this.answerImgUrl = builder.answerImgUrl;
        this.dateAnswer = builder.dateAnswer;
    }

    public static class Builder implements CommonModelBuilder<AskAnswer> {
        private final Member member;
        private final MemberAsk memberAsk;
        private final String answerTitle;
        private final String answerContent;
        private final String answerImgUrl;
        private final LocalDateTime dateAnswer;

        public Builder(Member member, MemberAsk memberAsk, AskAnswerRequest request) {
            this.member = member;
            this.memberAsk = memberAsk;
            this.answerTitle = request.getAnswerTitle();
            this.answerContent = request.getAnswerContent();
            this.answerImgUrl = null;
            this.dateAnswer = LocalDateTime.now();
        }

        public Builder(Member member, MemberAsk memberAsk, AskAnswerRequest request, String ImgUrl) {
            this.member = member;
            this.memberAsk = memberAsk;
            this.answerTitle = request.getAnswerTitle();
            this.answerContent = request.getAnswerContent();
            this.answerImgUrl = ImgUrl;
            this.dateAnswer = LocalDateTime.now();
        }



        @Override
        public AskAnswer build() {
            return new AskAnswer(this);
        }
    }
}

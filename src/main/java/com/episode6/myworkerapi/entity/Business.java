package com.episode6.myworkerapi.entity;


import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.business.BusinessChangeRequest;
import com.episode6.myworkerapi.model.business.BusinessLocationRequest;
import com.episode6.myworkerapi.model.business.BusinessRequest;
import com.episode6.myworkerapi.model.request.RefuseReasonRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Business {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId" , nullable = false)
    private Member member;

    @Column(nullable = false, length = 30)
    private String businessName;

    @Column(nullable = false, length = 12 , unique = true)
    private String businessNumber;

    @Column(nullable = false, length = 20)
    private String ownerName;

    @Column(nullable = false)
    private String businessImgUrl;

    @Column(nullable = false, length = 30)
    private String businessType;

    @Column(nullable = false, length = 60)
    private String businessLocation;

    @Column(nullable = false, length = 50)
    private String businessEmail;

    @Column(nullable = false, length = 13)
    private String businessPhoneNumber;

    @Column(nullable = false, length = 60)
    private String reallyLocation;

    private Double latitudeBusiness;

    private Double longitudeBusiness;

    @Column(nullable = false)
    private Boolean isActivity;

    private LocalDateTime dateJoinBusiness;

    @Column(nullable = false)
    private Boolean isApprovalBusiness;

    private LocalDateTime dateApprovalBusiness;

    @Column(length = 40)
    private String refuseReason;

    @Column(length = 40)
    private String refuseFix;



    /**
     * 수정 요청 수락 , 수정 내용 변경 후 삭제 (프론트에서 기본값 넣는 것 해야함)
     */
    public void PutBusinessFix(RequestFix requestFix) {
        this.businessName = requestFix.getBusinessName();
        this.ownerName = requestFix.getOwnerName();
        this.businessImgUrl = requestFix.getBusinessImgUrl();
        this.businessType = requestFix.getBusinessType();
        this. businessLocation = requestFix.getBusinessLocation();
        this.businessEmail = requestFix.getBusinessEmail();
        this. businessPhoneNumber = requestFix.getBusinessPhoneNumber();
        this.reallyLocation = requestFix.getReallyLocation();
    }
    /**
     * 사업장 요청 거절 , 거절 사유 등록
     */
    public void putRefuseReason(RefuseReasonRequest refuseReasonRequest) {
        this.refuseReason = refuseReasonRequest.getRefuseReason();
    }

    /**
     * 요청 수락 , 사업장 승인여부 true / 날짜 등록 후 리퀘스트 삭제
     */
    public void putRequestAgree(){
        this.isApprovalBusiness = true;
        this.dateApprovalBusiness = LocalDateTime.now();
    }

    /**
     * 관리자용 사업장 수정
     */
    public void putBusinessChangeRequest(BusinessChangeRequest request) {
        this.businessName  = request.getBusinessName();
        this.businessNumber  = request.getBusinessNumber();
        this.ownerName  = request.getOwnerName();
        this.businessImgUrl  = request.getBusinessImgUrl();
        this.businessType  = request.getBusinessType();
        this.businessLocation  = request.getBusinessLocation();
        this.businessEmail  = request.getBusinessEmail();
        this.businessPhoneNumber  = request.getBusinessPhoneNumber();
        this.reallyLocation  = request.getReallyLocation();
        this.latitudeBusiness  = request.getLatitudeBusiness();
        this.longitudeBusiness  = request.getLongitudeBusiness();
        this.isActivity  = request.getIsActivity();
        this.isApprovalBusiness  = request.getIsApprovalBusiness();
        this.refuseReason = request.getRefuseReason();
    }

    /**
     * 사장님 사업장 실근무지 수정
     */
    public void putReallyLocation(BusinessLocationRequest request) {
        this.reallyLocation = request.getReallyLocation();
    }

    private Business(Builder builder) {
        this.member = builder.member;
        this.businessName = builder.businessName;
        this.businessNumber = builder.businessNumber;
        this.ownerName = builder.ownerName;
        this.businessImgUrl = builder.businessImgUrl;
        this.businessType = builder.businessType;
        this.businessLocation = builder.businessLocation;
        this.businessEmail = builder.businessEmail;
        this.businessPhoneNumber = builder.businessPhoneNumber;
        this.reallyLocation = builder.reallyLocation;
        this.latitudeBusiness = builder.latitudeBusiness;
        this.longitudeBusiness = builder.longitudeBusiness;
        this.isActivity = builder.isActivity;
        this.dateJoinBusiness = builder.dateJoinBusiness;
        this.isApprovalBusiness = builder.isApprovalBusiness;
        this.dateApprovalBusiness = builder.dateApprovalBusiness;
        this.refuseReason = builder.refuseReason;
        this.refuseFix = builder.refuseFix;

    }

    public static class Builder implements CommonModelBuilder<Business> {
        private final Member member;
        private final String businessName;
        private final String businessNumber;
        private final String ownerName;
        private final String businessImgUrl;
        private final String businessType;
        private final String businessLocation;
        private final String businessEmail;
        private final String businessPhoneNumber;
        private final String reallyLocation;
        private final Double latitudeBusiness;
        private final Double longitudeBusiness;
        private final Boolean isActivity;
        private final LocalDateTime dateJoinBusiness;
        private final Boolean isApprovalBusiness;
        private final LocalDateTime dateApprovalBusiness;
        private final String refuseReason;
        private final String refuseFix;


        public Builder(BusinessRequest request, Member member) {
            this.member = member;
            this.businessName = request.getBusinessName();
            this.businessNumber = request.getBusinessNumber();
            this.ownerName = request.getOwnerName();
            this.businessImgUrl = request.getBusinessImgUrl();
            this.businessType = request.getBusinessType();
            this.businessLocation = request.getBusinessLocation();
            this.businessEmail = request.getBusinessEmail();
            this.businessPhoneNumber = request.getBusinessPhoneNumber();
            this.reallyLocation = request.getReallyLocation();
            this.latitudeBusiness = null;
            this.longitudeBusiness = null;
            this.isActivity = true;
            this.dateJoinBusiness = LocalDateTime.now();
            this.isApprovalBusiness = false;
            this.dateApprovalBusiness = null;
            this.refuseReason = null;
            this.refuseFix = null;
        }
        @Override
        public Business build() {
            return new Business(this);
        }
    }
}

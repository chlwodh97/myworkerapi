package com.episode6.myworkerapi.entity;

import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.notice.NoticeChangeRequest;
import com.episode6.myworkerapi.model.notice.NoticeRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Notice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "memberId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Member member;

    @Column(nullable = false)
    private LocalDateTime dateNotice;

    @Column(nullable = false, length = 30)
    private String title;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String content;

    private String noticeImgUrl;

    public void putNotice(NoticeChangeRequest request) {
        this.title = request.getTitle();
        this.content = request.getContent();
        this.noticeImgUrl = request.getNoticeImgUrl();
    }

    private Notice(Builder builder) {
        this.member = builder.member;
        this.dateNotice = builder.dateNotice;
        this.title = builder.title;
        this.content = builder.content;
        this.noticeImgUrl = builder.noticeImgUrl;
    }

    public static class Builder implements CommonModelBuilder<Notice> {
        private final Member member;
        private final LocalDateTime dateNotice;
        private final String title;
        private final String content;
        private final String noticeImgUrl;

        public Builder(Member member, NoticeRequest request) {
            this.member = member;
            this.dateNotice = LocalDateTime.now();
            this.title = request.getTitle();
            this.content = request.getContent();
            this.noticeImgUrl = null;
        }

        public Builder(Member member, NoticeRequest request, String noticeImgUrl) {
            this.member = member;
            this.dateNotice = LocalDateTime.now();
            this.title = request.getTitle();
            this.content = request.getContent();
            this.noticeImgUrl = noticeImgUrl;
        }

        @Override
        public Notice build() {
            return new Notice(this);
        }
    }
}

package com.episode6.myworkerapi.entity;

import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.manual.ManualRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Manual {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId" , nullable = false)
    private Member member;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "businessId", nullable = false)
    private Business business;

    @Column(nullable = false)
    private LocalDate dateManual;

    @Column(nullable = false, length = 30)
    private String title;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String content;

    private String manualImgUrl;

    private Manual(Builder builder) {
        this.member = builder.member;
        this.business = builder.business;
        this.dateManual = builder.dateManual;
        this.title = builder.title;
        this.content = builder.content;
        this.manualImgUrl = builder.manualImgUrl;
    }


    public static class Builder implements CommonModelBuilder<Manual>{
        private final Member member;
        private final Business business;
        private final LocalDate dateManual;
        private final String title;
        private final String content;
        private final String manualImgUrl;



        public Builder(ManualRequest manualRequest, Member member, Business business) {
            this.member = member;
            this.business = business;
            this.dateManual = LocalDate.now();
            this.title = manualRequest.getTitle();
            this.content = manualRequest.getContent();
            this.manualImgUrl = null;
        }


        public Builder(ManualRequest manualRequest, Member member, Business business, String imgUrl) {
            this.member = member;
            this.business = business;
            this.dateManual = LocalDate.now();
            this.title = manualRequest.getTitle();
            this.content = manualRequest.getContent();
            this.manualImgUrl = imgUrl;
        }
        @Override
        public Manual build() {
            return new Manual(this);
        }
    }
}

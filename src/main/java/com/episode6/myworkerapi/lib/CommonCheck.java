package com.episode6.myworkerapi.lib;

public class CommonCheck {

    public static boolean checkUsername(String username) {
        String pattern = "^[a-zA-Z가-힣]{1}[a-zA-Z가-힣0-9]{4,19}$"; // 정규식
        return username.matches(pattern);
    }
}

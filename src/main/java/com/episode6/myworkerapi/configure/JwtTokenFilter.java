package com.episode6.myworkerapi.configure;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class JwtTokenFilter extends GenericFilterBean {
    private final  JwtTokenProvider jwtTokenProvider;
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String tokenFull = jwtTokenProvider.resolveToken((HttpServletRequest) request);
        String token = "";
        // 토큰이 없거나 "Bearer " 로 시작하지 않는 경우 그대로 전달한다.
        if (tokenFull == null || !tokenFull.startsWith("Bearer ")) {
            chain.doFilter(request, response);
            return;
        }
        // "Bearer " 접두사를 제거하고 토큰을 추출한다.
        token = tokenFull.split(" ")[1].trim();
        // 추출한 토큰의 유효성을 검증하고 유효하지 않은 경우 그대로 전달한다.
        if (!jwtTokenProvider.validateToken(token)) {
            chain.doFilter(request, response);
            return;
        }
        // 유효한 토큰일 경우, 해당 토큰을 사용하여 사용자 인증(Authentication)을 가져온다.
        Authentication authentication = jwtTokenProvider.getAuthentication(token);
        // 일회용 출입증을 고정시켜놓겠다.
        SecurityContextHolder.getContext().setAuthentication(authentication);
        // 이 필터의 처리가 끝나면 다음 필터로 요청을 전달한다.
        chain.doFilter(request, response);
    }
}

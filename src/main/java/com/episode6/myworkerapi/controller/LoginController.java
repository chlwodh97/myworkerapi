package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.enums.MemberType;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.model.login.LoginRequest;
import com.episode6.myworkerapi.model.login.LoginResponse;
import com.episode6.myworkerapi.service.LoginService;
import com.episode6.myworkerapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/login")
public class LoginController {
    private final LoginService loginService;

    @PostMapping("/web/admin")
    @Operation(summary = "관리자용 로그인")
    public SingleResult<LoginResponse> doLoginAdmin(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_MANAGEMENT, loginRequest, "WEB"));
    }

    @PostMapping("/web/boss")
    @Operation(summary = "사장님 웹 로그인")
    public SingleResult<LoginResponse> doLoginBossWeb(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_BOSS, loginRequest, "WEB"));
    }

    @PostMapping("/app/boss")
    @Operation(summary = "사장님 앱 로그인")
    public SingleResult<LoginResponse> doLoginBossApp(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_BOSS, loginRequest, "APP"));
    }

    @PostMapping("/app/user")
    @Operation(summary = "앱 알바생 로그인")
    public SingleResult<LoginResponse> doLoginGeneral(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_GENERAL, loginRequest, "APP"));
    }
}

package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.model.notice.NoticeChangeRequest;
import com.episode6.myworkerapi.model.notice.NoticeItem;
import com.episode6.myworkerapi.model.notice.NoticeRequest;
import com.episode6.myworkerapi.model.notice.NoticeResponse;
import com.episode6.myworkerapi.service.NoticeService;
import com.episode6.myworkerapi.service.ProfileService;
import com.episode6.myworkerapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/notice")
public class NoticeController {
    private final ProfileService profileService;
    private final NoticeService noticeService;

    @PostMapping(value = "/new", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "공지사항 등록")
    public CommonResult setNotice(@RequestParam(required = false) MultipartFile multipartFile ,NoticeRequest request) throws IOException {
        Member member = profileService.getDate();
        noticeService.setNotice(member, request,multipartFile);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "공지사항 리스트 보기 페이징")
    public ListResult<NoticeItem> getNotices(@PathVariable int pageNum) {
        return ResponseService.getListResult(noticeService.getNotices(pageNum), true);
    }

    @GetMapping("/detail/notice-id/{noticeId}")
    @Operation(summary = "공지사항 상세보기")
    public SingleResult<NoticeResponse> getNotice(@PathVariable long noticeId) {
        return ResponseService.getSingleResult(noticeService.getNotice(noticeId));
    }

    @PutMapping("/change/notice-id/{noticeId}")
    @Operation(summary = "공지사항 수정")
    public CommonResult putNotice(@PathVariable long noticeId, @RequestBody NoticeChangeRequest request) {
        noticeService.putNotice(noticeId, request);
        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/notice-id/{noticeId}")
    @Operation(summary = "공지사항 삭제")
    public CommonResult delNotice(@PathVariable long noticeId) {
        noticeService.delNotice(noticeId);
        return ResponseService.getSuccessResult();
    }
}

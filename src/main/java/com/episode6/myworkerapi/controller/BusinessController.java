package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.business.*;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.service.BusinessService;
import com.episode6.myworkerapi.service.MemberService;
import com.episode6.myworkerapi.service.ProfileService;
import com.episode6.myworkerapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/business")
public class BusinessController {
    private final BusinessService businessService;
    private final MemberService memberService;
    private final ProfileService profileService;

    @PostMapping("join")
    @Operation(summary = "사업장 등록")
    public CommonResult setBusiness(@RequestBody BusinessRequest request) {
        Member member = profileService.getDate();
        businessService.setBusiness(request,member);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("all/{pageNum}")
    @Operation(summary = "사업장 최신순 페이징")
    public ListResult<BusinessItem> getBusinessPage(@PathVariable int pageNum) {
        return ResponseService.getListResult(businessService.getBusinessPage(pageNum), true);
    }

    @GetMapping("detail/business-id/{businessId}")
    @Operation(summary = "사업장 상세보기")
    public SingleResult<BusinessResponse> getBusiness(@PathVariable long businessId) {
        return ResponseService.getSingleResult(businessService.getBusiness(businessId));
    }

    @PutMapping("put/business-id/{businessId}")
    @Operation(summary = "관리자용 사업장 수정")
    public CommonResult putBusiness(@RequestBody BusinessChangeRequest request, @PathVariable long businessId) {
        businessService.putBusiness(businessId,request);
        return ResponseService.getSuccessResult();
    }

    @PutMapping("putLocation")
    @Operation(summary = "사장님 실근무지 수정")
    public CommonResult putBusinessLocation(@RequestBody BusinessLocationRequest request) {
        Member member = profileService.getDate();
        businessService.putBusinessLocation(member,request);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("detail/my/business")
    @Operation(summary = "사장님 내 사업장 보기")
    public SingleResult<BusinessResponse> getMyBusiness() {
        Member member = profileService.getDate();
        return ResponseService.getSingleResult(businessService.getMyBusiness(member));
    }

    @GetMapping("detail/business/member/business-id/{businessId}")
    @Operation(summary = "사업장과 알바생리스트")
    public SingleResult<BusinessDetailMemberResponse> getBusinessOfMember(@PathVariable long businessId) {
        Business business = businessService.getBusinessData(businessId);

        return ResponseService.getSingleResult(businessService.getBusinessOfMember(businessId));
    }
}

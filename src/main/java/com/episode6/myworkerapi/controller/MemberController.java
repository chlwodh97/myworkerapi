package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.enums.MemberType;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.model.member.*;
import com.episode6.myworkerapi.service.MemberService;
import com.episode6.myworkerapi.service.ProfileService;
import com.episode6.myworkerapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final ProfileService profileService;
    private final MemberService memberService;

    @GetMapping("/check/id")
    @Operation(summary = "유저네임 중복확인")
    public SingleResult<MemberDupCheckResponse> getMemberDupCheck(@RequestParam(name = "username") String username) {
        return ResponseService.getSingleResult(memberService.getMemberIdDupCheck(username));
    }
    @PostMapping("/join")
    @Operation(summary = "회원가입")
    public CommonResult setMember(@RequestBody @Valid MemberRequest request) {
        memberService.setMember(request.getMemberType(), request);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "회원 리스트로 보기 최신순")
    public ListResult<MemberItem> getMembers(@PathVariable int pageNum) {
        return ResponseService.getListResult(memberService.getMembers(pageNum), true);
    }

    @GetMapping("/detail/id/{id}")
    @Operation(summary = "회원 정보 상세보기")
    public SingleResult<MemberResponse> getMember(@PathVariable long id) {
        return ResponseService.getSingleResult(memberService.getMember(id));
    }

    @GetMapping("/detail")
    @Operation(summary = "내 정보 상세보기")
    public SingleResult<MemberResponse> getMember() {
        Member member = profileService.getDate();
        return ResponseService.getSingleResult(memberService.getMember(member));
    }

    @PutMapping("/password/change/id/{id}")
    @Operation(summary = "비밀번호와 비밀번호 확인이 맞을경우 변경")
    public CommonResult putPassword(@PathVariable long id, @RequestBody @Valid MemberPasswordCheckRequest request) throws Exception {
        memberService.putPassword(id, request);
        return ResponseService.getSuccessResult();
    }

    @PutMapping("/address/change/id/{id}")
    @Operation(summary = "회원 정보 변경")
    public CommonResult putMemberChange(@PathVariable long id, @RequestBody @Valid MemberChangeRequest request) {
        memberService.putMemberChange(id, request);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/owner/{pageNum}")
    @Operation(summary = "사업자 회원 최신순 페이징")
    public ListResult<MemberItem> getOwnerMember(@PathVariable int pageNum) {
        return ResponseService.getListResult(memberService.getOwnerMember(pageNum),true);
    }

    @GetMapping("/all/general/{pageNum}")
    @Operation(summary = "일반 회원 최신순 페이징")
    public ListResult<MemberItem> getGeneralMember(@PathVariable int pageNum) {
        return ResponseService.getListResult(memberService.getGeneralMember(pageNum),true);
    }

    @GetMapping("/search")
    @Operation(summary = "회원 정보 아이디로 검색")
    public SingleResult<MemberResponse> getMemberSearch(@RequestParam(name = "memberType")MemberType memberType, @RequestParam(name = "username") String username) {
        return ResponseService.getSingleResult(memberService.getMemberSearch(memberType, username));
    }

    @GetMapping("/get/boss-business/member-id/{memberId}")
    @Operation(summary = "사장님과 사업장 같이 보기")
    public SingleResult<MemberBossDetailResponse> getMyBusinessDetail(@PathVariable long memberId) {
        Member member = memberService.getMemberData(memberId);

        return ResponseService.getSingleResult(memberService.getMyBusinessDetail(member));
    }
    @GetMapping("/get/general-business/member-id/{memberId}")
    @Operation(summary = "알바생 사업장 같이 보기")
    public SingleResult<MemberGeneralDetailResponse> getMyBusinessGeneralDetail(@PathVariable long memberId) {
        Member member = memberService.getMemberData(memberId);

        return ResponseService.getSingleResult(memberService.getMyBusinessGeneralDetail(memberId));
    }

}

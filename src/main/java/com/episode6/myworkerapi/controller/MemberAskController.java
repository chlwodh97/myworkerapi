package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.memberask.MemberAskItem;
import com.episode6.myworkerapi.model.memberask.MemberAskRequest;
import com.episode6.myworkerapi.model.memberask.MemberAskResponse;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.service.MemberAskService;
import com.episode6.myworkerapi.service.ProfileService;
import com.episode6.myworkerapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member-ask")
public class MemberAskController {
    private final ProfileService profileService;
    private final MemberAskService memberAskService;

    @PostMapping(value = "/new", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "문의사항 등록")
    public CommonResult setMemberAsk(@RequestParam(required = false)MultipartFile multipartFile, MemberAskRequest request) throws IOException {
        Member member = profileService.getDate();
        memberAskService.setMemberAsk(member,request, multipartFile);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "문의사항 리스트로 보기 페이징")
    public ListResult<MemberAskItem> getAsks(@PathVariable int pageNum) {
        return ResponseService.getListResult(memberAskService.getAsks(pageNum),true);
    }

    @GetMapping("/all/member/{pageNum}")
    @Operation(summary = "회원이 쓴 문의사항 리스트로 보기 페이징")
    public ListResult<MemberAskItem> getAskMembers(@PathVariable int pageNum) {
        Member member = profileService.getDate();
        return ResponseService.getListResult(memberAskService.getAskMembers(member, pageNum), true);
    }

    @GetMapping("/detail/member-ask-id/{memberAskId}")
    @Operation(summary = "문의사항 상세보기")
    public SingleResult<MemberAskResponse> getAsk(@PathVariable long memberAskId) {
        return ResponseService.getSingleResult(memberAskService.getAsk(memberAskId));
    }

    @DeleteMapping("/del/member-ask-id/{memberAskId}")
    @Operation(summary = "문의사항 삭제")
    public CommonResult delAnswer(@PathVariable long memberAskId) {
        memberAskService.delAnswer(memberAskId);
        return ResponseService.getSuccessResult();
    }
}

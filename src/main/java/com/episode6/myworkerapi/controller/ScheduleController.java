package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.schedule.ScheduleEtcRequest;
import com.episode6.myworkerapi.model.schedule.ScheduleItem;
import com.episode6.myworkerapi.model.schedule.ScheduleRequest;
import com.episode6.myworkerapi.service.*;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/schedule")
public class ScheduleController {
    private final BusinessService businessService;
    private final ProfileService profileService;
    private final ScheduleService scheduleService;

    @PostMapping("/new/business-id/{businessId}")
    @Operation(summary = "출근 등록")
    public CommonResult setSchedule(@PathVariable long businessId, @RequestBody @Valid ScheduleRequest request) {
        Member member = profileService.getDate();
        Business business = businessService.getBusinessData(businessId);
        scheduleService.setSchedule(business, member, request);
        return ResponseService.getSuccessResult();
    }

    @PutMapping("/end-work/business-id/{businessId}")
    @Operation(summary = "퇴근 하기")
    public CommonResult putEndWork(@PathVariable long businessId, @RequestBody @Valid ScheduleRequest request) {
        Member member = profileService.getDate();
        Business business = businessService.getBusinessData(businessId);
        scheduleService.putEndWork(business, member, request);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "출근 리스트 보기 관리자용")
    public ListResult<ScheduleItem> getSchedules(@PathVariable int pageNum) {
        return ResponseService.getListResult(scheduleService.getSchedules(pageNum), true);
    }

    @GetMapping("/all/business-id/{businessId}/{pageNum}")
    @Operation(summary = "사업장 알바생들 출퇴근 보기")
    public ListResult<ScheduleItem> getScheduleBusiness(@PathVariable long businessId, @PathVariable int pageNum) {
        Business business = businessService.getBusinessData(businessId);
        return ResponseService.getListResult(scheduleService.getScheduleBusiness(business, pageNum), true);
    }

    @GetMapping("/all/member/business-id/{businessId}/{pageNum}")
    @Operation(summary = "알바생 사업장하나 출퇴근 리스트 보기")
    public ListResult<ScheduleItem> getScheduleBusinessMember(@PathVariable long businessId, @PathVariable int pageNum) {
        Member member = profileService.getDate();
        Business business = businessService.getBusinessData(businessId);
        return ResponseService.getListResult(scheduleService.getScheduleBusinessMember(business, member, pageNum), true);
    }

    @GetMapping("/all/member/business-full/{pageNum}")
    @Operation(summary = "알바생이 다니는 모든 사업장 출퇴근 리스트")
    public ListResult<ScheduleItem> getScheduleBusinessFull(@PathVariable int pageNum) {
        Member member = profileService.getDate();
        return ResponseService.getListResult(scheduleService.getScheduleBusinessMemberAll(member, pageNum), true);
    }

    @GetMapping("/calendar")
    @Operation(summary = "사업장 출퇴근 한달 캘린더 사장님용")
    public ListResult<ScheduleItem> getScheduleCalendar(@RequestParam(name = "year") int year, @RequestParam(name = "month") int month) {
        Member member = profileService.getDate();
        Business business = businessService.getBusinessData(member);
        return ResponseService.getListResult(scheduleService.getScheduleCalendar(business, year, month), true);
    }

    @GetMapping("/calendar/day")
    @Operation(summary = "사업장 출퇴근 하루 캘린더 사장님용")
    public ListResult<ScheduleItem> getScheduleCalendarDay(@RequestParam(name = "today") LocalDate today) {
        Member member = profileService.getDate();
        Business business = businessService.getBusinessData(member);
        return ResponseService.getListResult(scheduleService.getScheduleCalendarDay(business, today), true);
    }

    @GetMapping("/calendar/business-id/{businessId}")
    @Operation(summary = "사업장하나 출퇴근 한달 캘린더 알바생용")
    public ListResult<ScheduleItem> getScheduleCalendar(@PathVariable long businessId, @RequestParam(name = "year") int year, @RequestParam(name = "month") int month) {
        Member member = profileService.getDate();
        Business business = businessService.getBusinessData(businessId);
        return ResponseService.getListResult(scheduleService.getScheduleCalendar(business, member, year, month), true);
    }

    @GetMapping("/calendar/day/business-id/{businessId}")
    @Operation(summary = "사업장하나 출퇴근 하루 캘린더 알바생용")
    public ListResult<ScheduleItem> getScheduleCalendarDay(@PathVariable long businessId,@RequestParam(name = "today") LocalDate today) {
        Member member = profileService.getDate();
        Business business = businessService.getBusinessData(businessId);
        return ResponseService.getListResult(scheduleService.getScheduleCalendarDay(business, member, today), true);
    }

    @PutMapping("/etc/schedule-Id/{scheduleId}")
    @Operation(summary = "메모 등록 및 수정")
    public CommonResult putScheduleEtc(@PathVariable long scheduleId, @RequestBody @Valid ScheduleEtcRequest request) {
        scheduleService.putScheduleEtc(scheduleId, request);
        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/{scheduleId}")
    @Operation(summary = "출퇴근 삭제")
    public CommonResult delSchedule(@PathVariable long scheduleId) {
        scheduleService.delSchedule(scheduleId);
        return ResponseService.getSuccessResult();
    }


}

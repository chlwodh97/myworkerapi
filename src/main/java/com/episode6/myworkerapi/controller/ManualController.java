package com.episode6.myworkerapi.controller;


import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.model.manual.ManualItem;
import com.episode6.myworkerapi.model.manual.ManualRequest;
import com.episode6.myworkerapi.model.manual.ManualResponse;
import com.episode6.myworkerapi.service.*;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.awt.*;
import java.io.IOException;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/manual")
public class ManualController {
    private final ProfileService profileService;
    private final BusinessService businessService;
    private final ManualService manualService;

    @PostMapping(value = "new/business-id/{businessId}", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "메뉴얼 등록 알바생용")
    public CommonResult setManual(@RequestParam(required = false)MultipartFile multipartFile, ManualRequest request, @PathVariable long businessId) throws IOException {
        Member member = profileService.getDate();
        Business business = businessService.getBusinessData(businessId);
        manualService.setManual(request, member, business, multipartFile);
        return ResponseService.getSuccessResult();
    }
    @PostMapping(value = "new/", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "메뉴얼 등록 사장님용")
    public CommonResult setBossManual(@RequestParam(required = false)MultipartFile multipartFile, ManualRequest request) throws IOException {
        Member member = profileService.getDate();
        Business business = businessService.getBusinessData(member);
        manualService.setManual(request, member, business,multipartFile);
        return ResponseService.getSuccessResult();
    }
    @GetMapping("all/business/{pageNum}/business-id/{businessId}")
    @Operation(summary = "메뉴얼 최신순 페이징 _알바생용")
    public ListResult<ManualItem> getManuals(@PathVariable int pageNum ,@PathVariable long businessId) {
        Business business = businessService.getBusinessData(businessId);
        return ResponseService.getListResult(manualService.getManuals(pageNum, business),true);
    }

    @GetMapping("all/boss/{pageNum}")
    @Operation(summary = "메뉴얼 최신순 페이징 _사장님용")
    public ListResult<ManualItem> getManuals(@PathVariable int pageNum) {
        Member member = profileService.getDate();
        Business business = businessService.getBusinessData(member);
        return ResponseService.getListResult(manualService.getManuals(pageNum, business),true);
    }

    @GetMapping("all/{pageNum}")
    @Operation(summary = "메뉴얼 최신순 페이징 _관리자")
    public ListResult<ManualItem> getAllManuals(@PathVariable int pageNum) {
        return ResponseService.getListResult(manualService.getAllManuals(pageNum),true);
    }

    @GetMapping("detail/manual-id/{manualId}")
    @Operation(summary = "메뉴얼 상세보기")
    public SingleResult<ManualResponse> getManual(@PathVariable long manualId) {
        return ResponseService.getSingleResult(manualService.getManual(manualId));
    }
}

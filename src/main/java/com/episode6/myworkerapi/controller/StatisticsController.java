package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.nearDayStatics.NearDayStaticsResponse;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.model.statistics.StatisticsScheduleResponse;
import com.episode6.myworkerapi.service.BusinessService;
import com.episode6.myworkerapi.service.ProfileService;
import com.episode6.myworkerapi.service.ResponseService;
import com.episode6.myworkerapi.service.StatisticsService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/chart")
public class StatisticsController {
    private final ProfileService profileService;
    private final BusinessService businessService;
    private final StatisticsService statisticsService;

    @GetMapping("day-join-chart")
    @Operation(summary = "하루 차트")
    public SingleResult<NearDayStaticsResponse> getNearStatistics() {

        return ResponseService.getSingleResult(statisticsService.getNearStatistics());
    }

    @GetMapping("month-join-chart")
    @Operation(summary = "한달 차트")
    public SingleResult<NearDayStaticsResponse> getMonthStatistics() {

        return ResponseService.getSingleResult(statisticsService.getMonthStatistics());
    }

    @GetMapping("/schedule/chart")
    @Operation(summary = "오늘 하루 출근하는 알바생 내역")
    public SingleResult<StatisticsScheduleResponse> getDashBoard() {
        Member member = profileService.getDate();
        Business business = businessService.getBusinessData(member);
        return ResponseService.getSingleResult(statisticsService.getDashBoard(business));
    }


}

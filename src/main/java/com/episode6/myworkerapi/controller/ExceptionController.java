package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.exception.CAccessDeniedException;
import com.episode6.myworkerapi.exception.CEntryPointException;
import com.episode6.myworkerapi.model.common.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exception")
public class ExceptionController {

    @GetMapping("/access-denied")
    public CommonResult accessDeniedException() {
        throw new CAccessDeniedException();
    }
    @GetMapping("/entry-point")
    public CommonResult entryPointException() {
        throw new CEntryPointException();
    }
}

package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.entity.Product;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.productrecord.ProductRecordItem;
import com.episode6.myworkerapi.model.productrecord.ProductRecordRequest;
import com.episode6.myworkerapi.service.*;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/product-record")
public class ProductRecordController {
    private final ProfileService profileService;
    private final ProductService productService;
    private final BusinessService businessService;
    private final ProductRecordService productRecordService;

    @PostMapping("/product-id/{productId}/business-id/{businessId}")
    @Operation(summary = "재고 수정 등록 알바생용")
    public CommonResult setProductRecord(@PathVariable long productId, @PathVariable long businessId, @RequestBody ProductRecordRequest request) {
        Member member = profileService.getDate();
        Product product = productService.getProductData(productId);
        Business business = businessService.getBusinessData(businessId);
        productRecordService.setProductRecord(product, business, member, request);
        return ResponseService.getSuccessResult();
    }

    @PostMapping("/boss/product-id/{productId}")
    @Operation(summary = "재고 수정 등록 사장님용")
    public CommonResult setProductRecord(@PathVariable long productId, @RequestBody ProductRecordRequest request) {
        Member member = profileService.getDate();
        Product product = productService.getProductData(productId);
        Business business = businessService.getBusinessData(member);
        productRecordService.setProductRecord(product, business, member, request);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "재고 수정 리스트 관리자용?")
    public ListResult<ProductRecordItem> getProductRecords(@PathVariable int pageNum) {
        return ResponseService.getListResult(productRecordService.getProductRecords(pageNum), true);
    }
}

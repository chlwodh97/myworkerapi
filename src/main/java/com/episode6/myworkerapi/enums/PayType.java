package com.episode6.myworkerapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PayType {
    TIME_WAGE("시급"),
    DAY_WAGE("일급"),
    WEEK_WAGE("주급"),
    MONTH_WAGE("월급"),
    YEAR_WAGE("연봉");

    private final String name;
}

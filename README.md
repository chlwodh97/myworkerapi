# 내 알바야 프로젝트
### 팀명: episode6 (6인)
### 프로젝트 소개
![사용한 기술](./images/ckddjq.png)
![사용한 기술](./images/dkfqk.png)
* 이 프로젝트는 혼자서 알바생을 관리하기 힘든 사장님들을 위한 프로젝트입니다.
* 사장님의 매장의 인수인계 사항이나, 메뉴얼 등을 쉽게 전달할 수 있습니다.
* 알바생 앱에서는 위도, 경도에 따른 위치기반으로 출퇴근을 체크합니다.
* 근로계약서를 앱에 저장하고 관리할 수 있습니다.
### 개발기간 : 2024 - 03.06 ~ 05.08


### SWOT 분석
![사용한 기술](./images/swot.png)

### 백엔드 개발 참여자
* **김기범, 최재오**
### 사용한 기술
* **Spring boot, Docker, PostgreSQL, Google Cloud Platform(GCP)**
### 프로젝트 아키텍처
![사용한 기술](./images/ProjectArchitecture.png)
### 프로젝트 주요 기능

**스케줄 관리**

알바생의 출근 일정이나 출근 시간을 캘린더로 한눈에 볼 수 있습니다.
출퇴근 관리와 출근 여부를 볼 수 있습니다.
GPS 기능으로 실 근무지와 현재위치를 체크해 출퇴근을 체크합니다.
캘린더를 사용해 한눈에 근무 스케줄을 확인 할 수있습니다.

**계약셔 관리**

근로계약서를 앱 내 작성, 저장할 수 있습니다.

**인수인계, 메뉴얼**

사업장 내 지켜야 할 메뉴얼이나 인수인계 사항등을 편하게 관리할 수 있습니다.

### 관련링크
- <a href="https://flowery-clementine-57f.notion.site/Project2-MyWoker-d36eed82e134480da0e10065c95359ba">자세한 설명 링크</a>
- <a href="https://www.youtube.com/watch?v=TT2fAmvc3mc&t=3s">유튜브 영상(사장님)</a>
- <a href="https://www.youtube.com/watch?v=-GLuWnb4N2M&t=6s">유튜브 영상(알바생 + 관리자)</a>